<div class="user-activity-tabs">
    <ul>
        @if(Auth::check())
            @if(Auth::user()->id == $user->id)
                <li><a href="{{ route('notifications', ['usernmae' => Auth::user()->username]) }}" {{ strpos(Request::url(), 'notification') ? ' class="active"' : null }} title="Posts">Notification<sup class="activity-count">{{ $count['notification'] }}</sup></a></li>
            @endif
        @endif
        <li><a href="{{ route('user-post', ['usernmae' => $user->username]) }}" {{ strpos(Request::url(), 'post') ? ' class="active"' : null }} title="Posts">Posts<sup class="activity-count">{{ $count['news'] }}</sup></a></li>
        <li><a href="{{ route('user-question', ['usernmae' => $user->username]) }}" {{ strpos(Request::url(), 'question') ? ' class="active"' : null }} title="Questions">Questions<sup class="activity-count">{{ $count['question'] }}</sup></a></li>
        <li><a href="{{ route('users-collections', ['usernmae' => $user->username]) }}" {{ strpos(Request::url(), 'collection') ? ' class="active"' : null }} title="Collections">Collections<sup class="activity-count">{{ $count['collection'] }}</sup></a></li>
    </ul>
</div><!-- .user-activity-tabs -->