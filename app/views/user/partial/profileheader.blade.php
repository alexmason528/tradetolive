<!-- <header id="site-header" class="site-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 header-left">
                <a class="back-button" href="{{ URL::previous() }}" title="Back to listing"><i class="icon icon-arrow-left"></i>Back</a>
            </div>

            <div class="col-md-6 page-title">
               <h1>{{ t('Profile Settings') }}</h1>
            </div>
            
            <div class="col-md-3 header-right"></div>
        </div>
    </div>
</header> -->

<header id="site-header" class="site-header">
    <!-- Note: add dynamic post background image url in the style attribute above -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 header-left"> 
                <a class="back-button" href="{{ URL::previous() }}" title="Back"><i class="icon icon-arrow-left"></i>Back</a>
            </div><!-- .header-left -->

            <div class="col-md-6 page-title">

                <div class="user-profile clearfix">
                    <div class="user-image"> 
                        <img src="{{ getAvatar($user) }}" alt="{{ ucfirst($user->fullname) }}">
                        <span class="user-followings">{{ $user->followers->count() }}</span>
                    </div><!-- .author-image -->
                    <div class="user-info">
                        <h1>{{ ucfirst($user->fullname) }}</h1>
                        <div class="user-meta">
                            <span class="user-followed-by">Followed by: {{ $user->followers->count() }}</span>
                            <span class="dot-sap"><i class="icon icon-dot"></i></span>
                            <span class="user-following">Following: {{ $user->following->count() }}</span>
                        </div><!-- .user-meta -->
                    </div><!-- .user-info -->
                </div><!-- .user-profile -->

                <div class="follow-user">
                    @if(Auth::check())
                        @if(Auth::user()->id !== $user->id)
                            @if(Auth::user()->following()->whereFollowId($user->id)->count() < 1)
                                <button class="btn btn-info follow follow-btn" data-id="{{ $user->id }}"><i class="icon icon-follow"></i> {{ t('Follow Me') }}</button>
                            @else
                                <button class="btn btn-danger follow follow-btn" data-id="{{ $user->id }}"><i class="icon icon-follow"></i> {{ t('Un Follow') }}</button>
                            @endif
                        @endif
                    @endif
                
                    <!-- <a href="#" class="follow-button" title="Follow this user"><i class="icon icon-follow"></i>Follow</a> -->
                </div>
                
            </div><!-- .page-title -->
            
            <div class="col-md-3 header-right">
                
            </div><!-- .header-right -->
        </div><!-- .row -->
    </div><!-- .container-fluid -->                
</header><!-- #site-header -->