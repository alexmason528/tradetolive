@extends('master/index')
@section('meta_title')
   {{{ ucfirst($user->fullname) }}}'s Profile - {{ siteSettings('siteName') }}
@stop
@section('page-content')
   <div id="site-content" class="site-content collections">
      @include('master/header')
      <header id="site-header" class="site-header">
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-3 header-left">
                  <a class="back-button" href="{{ URL::previous() }}" title="Back"><i class="icon icon-arrow-left"></i>Back</a>
               </div>

               <div class="col-md-6 page-title">
                     <h1>{{ t('Followers') }}</h1>
               </div><!-- .page-title -->
                
               <div class="col-md-3 header-right"><!-- Blank column--></div><!-- .header-right -->
            </div><!-- .row -->
         </div><!-- .container-fluid -->                
      </header><!-- #site-header -->

      <div class="page-content">
         <div class="container-fluid">
            <div class="row">
               <div class="col-md-3"></div>
               <div class="col-md-6">
                  <div class="articles-list">
                     @foreach($followers as $user)
                        <div class="row">
                           <div class="col-md-6">
                              <div class="row">
                                 <div class="col-md-2 col-sm-2 pull-left" style="margin-bottom:15px;min-wdth:100px">
                                    <a href="{{ url('user/'.$user->username) }}"><img class="img-circle img-responsive" src="{{ getAvatar($user,114,114) }}"></a>
                                 </div>
                                 <div class="col-md-8">
                                    <h3 style="margin-top:0px"><a href="{{ route('user', ['usernmae' => $user->username]) }}">{{{ ucfirst($user->fullname) }}}</a></h3>
                                    <p>
                                       <i class="glyphicon glyphicon-comment"></i> {{ $user->comments->count() }} {{t('comments')}} &middot; <i class="glyphicon glyphicon-share-alt"></i> {{ $user->posts->count() }} {{ t('Posts') }}
                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     @endforeach
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
@stop
@section('pagination')
   <div class="row">{{ $followers->links() }}</div>
@stop