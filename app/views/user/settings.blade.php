@extends('master/index')

@section('page-content')
    <div id="site-content" class="site-content user-view">
        @include('master/header')
        @include('user.partial.profileheader')

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        @if(extension_loaded('fileinfo'))
                            {{ Form::open(['class'=>'default-form', 'url' => 'settings/avatar', 'files' => true,'role'=>'form']) }}
                            <div class="form-group">
                                <label for="avatar">{{ t('Avatar') }}</label>
                                <input type="file" name="avatar"/>
                            </div>
                            <div class="form-group">
                                {{ Form::submit(t('Update Avatar'),['class'=>'btn btn-default btn-success']) }}
                            </div>
                            {{ Form::close() }}

                            <h3 class="page-header">{{ t('Basic Settings')}}</h3>

                        @endif

                        {{ Form::open(['class'=>'default-form', 'url'=>'settings/profile']) }}
                        <div class="form-group">
                            <label for="fullname">{{ t('Fullname') }}</label>
                            {{ Form::text('fullname',Auth::user()->fullname,['placeholder' => t('Fullname'), 'required' => 'required']) }}
                        </div>

                        <div class="form-group">
                            <label for="gender">{{ t('Gender') }}<small>*</small></label>
                            {{ Form::select('gender', ['male' => 'Male', 'female' => 'Female'], Auth::user()->gender, ['id'=>'gender','required'=>'required']) }}
                        </div>

                        <div class="form-group">
                            <label for="blogurl">Facebook Profile
                                <small>( Your Facebook Profile or Page Link )</small>
                            </label>
                            {{ Form::text('fbLink',Auth::user()->fb_link) }}
                        </div>

                        <div class="form-group">
                            <label for="blogurl">Twitter Profile
                                <small>( Your Twitter Profile Link )</small>
                            </label>
                            {{ Form::text('twLink',Auth::user()->tw_link) }}
                        </div>

                        <div class="form-group">
                            <label for="blogurl">Blog url
                                <small>( Your blog or social profile url )</small>
                            </label>
                            {{ Form::text('blogurl',Auth::user()->blogurl) }}
                        </div>

                        <div class="form-group">
                            <p class="align-center">{{ Form::submit(t('Submit'),['class'=>'submit-button']) }}</p>
                        </div>

                        {{ Form::close() }}


                        <h3 class="page-header">{{ t('Email Settings')}}</h3>
                        {{ Form::open(['class'=>'default-form', 'url' => 'settings/mailsettings']) }}
                        <div class="form-group">
                            <label for="email_comment">{{ t('Email me when someone comment') }}</label>
                            {{ Form::checkbox('email_comment',1,Auth::user()->email_comment) }}
                        </div>
                        <div class="form-group">
                            <label for="email_reply">{{ t('Email me when someone reply on my comment') }}</label>
                            {{ Form::checkbox('email_reply',1,Auth::user()->email_reply) }}
                        </div>
                        <div class="form-group">
                            <label for="email_vote">{{ t('Email me when someone upvote my posts') }}</label>
                            {{ Form::checkbox('email_vote',1,Auth::user()->email_vote) }}
                        </div>
                        <div class="form-group">
                            <label for="email_follow">{{ t('Email me when someone follow me') }}</label>
                            {{ Form::checkbox('email_follow',1,Auth::user()->email_follow) }}
                        </div>
                        <div class="form-group">
                            <p class="align-center">{{ Form::submit(t('Update Mail Settings'),['class'=>'submit-button']) }}</p>
                        </div>
                        {{ Form::close() }}

                        <h3 class="page-header">{{ t('Change Password')}}</h3>
                        {{ Form::open(['class'=>'default-form', 'url'=>'settings/changepassword']) }}
                        <div class="form-group">
                            <label for="currentpassword">{{ t('Current Password') }}</label>
                            {{ Form::password('currentpassword',['required' => 'required']) }}
                        </div>
                        <div class="form-group">
                            <label for="password">{{ t('New Password') }}</label>
                            {{ Form::password('password',['required' => 'required']) }}
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">{{ t('Confirm Password') }}</label>
                            {{ Form::password('password_confirmation',['required' => 'required']) }}
                        </div>

                        <div class="form-group">
                            <p class="align-center">{{ Form::submit(t('Change Password'),['class'=>'submit-button']) }}</p>
                        </div>


                        {{ Form::close() }}
                    </div>

                    <div class="col-md-3"></div>                        
                </div>
            </div>
        </div>
    </div>
@stop