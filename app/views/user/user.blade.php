@extends('master/index')
@section('meta_title')
    {{{ ucfirst($user->fullname) }}}'s {{ t('Profile') }} - {{ siteSettings('siteName') }}
@stop
@section('page-content')
    <div id="site-content" class="site-content user-view">
        @include('master/header')
        @include('user.partial.profileheader')
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        @include('user.partial.subnav')
                        <div class="articles-list">
                        @foreach($posts as $post)
                            <article id="{{ $post->id }}" class="article-post">
                                <a href="{{ route($post->type,['id'=>$post->id,'slug'=>$post->slug]) }}" title="{{ ucfirst(e($post->title))}}" class="post-link">
                                    <div class="up-count">
                                        <i class="icon icon-arrow-up"></i>{{ $post->votes->count() }}
                                    </div><!-- .up-count -->
                                    <div class="post-info">
                                        <div class="post-info-wrapper">
                                            <cite>{{ parseUrl($post->url) }}</cite>
                                            <h3>{{ ucfirst(e($post->title)) }}</h3>
                                            <div class="post-meta">
                                                <span class="post-date"><abbr class="timeago" title="{{ $post->approved_at->toISO8601String() }}">{{ $post->approved_at->toDateTimeString() }}</abbr></span>
                                                <span class="post-catgories"><span>{{ $post->category->name }}</span></span>
                                                <span class="post-author">{{ ucfirst($post->user->fullname) }}</span>
                                            </div><!-- .post-meta -->
                                        </div>
                                    </div><!-- .post-info -->
                                    
                                    <div class="post-data">
                                        <span class="comments-count"><i class="icon icon-comments"></i>{{ $post->comments->count() }}</span>
                                        <span class="view-count"><i class="icon icon-view"></i>{{ $post->views }}</span>
                                    </div><!-- .post-data -->
                                </a><!-- .post-link -->
                            </article><!-- .article-post -->
                        @endforeach
                        </div>
                        <!-- <p><i class="fa fa-flag-o flag"></i> {{ link_to_route('flag-user',t('Flag'),['username'=>$user->username],['class'=>'flag']) }}</p> -->
                    </div>

                    <div class="col-md-3"></div>                        
                </div>
            </div>
        </div>
    </div>

    
@stop

@section('pagination')
    {{ $posts->links() }}
@stop