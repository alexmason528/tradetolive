@extends('master/index')
@section('meta_title')
    {{{ ucfirst($collection->title) }}} - {{ siteSettings('siteName') }}
@stop
@section('page-content')
    <div id="site-content" class="site-content collections">
        @include('master/header')
        <header id="site-header" class="site-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 header-left">
                        <a class="back-button" href="{{ URL::previous() }}" title="Back to listing"><i class="icon icon-arrow-left"></i>Back</a>
                    </div>

                    <div class="col-md-6 page-title">
                       <h1>{{ t('Collections') }}</h1>
                    </div>
                    
                    <div class="col-md-3 header-right"></div>
                </div>
            </div>
        </header>

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="collection-header">
                            <h2>{{{ ucfirst(($collection->title)) }}}</h2>
                            <p>{{{ $collection->summary }}}</p>
                            <p><i class="fa fa-pencil fa-fw"></i>{{ $posts->count() }} {{{ t('posts') }}} <i class="fa fa-eye fa-fw"></i> {{ $collection->views }} {{{ t('views') }}}
                                @if(Auth::check())
                                    @if(Auth::user()->username == $user->username)
                                        <i class="fa fa-edit fa-fw"></i> {{ link_to_route('users-collection-edit', t('Edit'), ['username' => $collection->user->username, 'slug'=> $collection->slug]) }}
                                    @endif
                                @endif
                            </p>
                        </div>
                        <div class="articles-list">
                        @foreach($posts as $collection)
                            <article id="{{ $collection->posts->id }}" class="article-post">
                                <a href="{{ route($collection->posts->type,['id'=>$collection->posts->id,'slug'=>$collection->posts->slug]) }}" title="{{ ucfirst(e($collection->posts->title))}}" class="post-link">
                                    <div class="up-count">
                                        <i class="icon icon-arrow-up"></i>{{ $collection->posts->votes->count() }}
                                    </div><!-- .up-count -->

                                    <div class="post-info">
                                        <div class="post-info-wrapper">
                                            <cite>{{ parseUrl($collection->posts->url) }}</cite>
                                            <h3>{{ ucfirst(e($collection->posts->title)) }}</h3>
                                            <div class="post-meta">
                                                <span class="post-date"><abbr class="timeago" title="{{ $collection->posts->approved_at->toISO8601String() }}">{{ $collection->posts->approved_at->toDateTimeString() }}</abbr></span>
                                                <span class="post-catgories"><span>{{ $collection->posts->category->name }}</span></span>
                                                <span class="post-author">{{ ucfirst($collection->posts->user->fullname) }}</span>
                                            </div><!-- .post-meta -->
                                        </div>
                                    </div><!-- .post-info -->
                                    
                                    <div class="post-data">
                                        <span class="comments-count"><i class="icon icon-comments"></i>{{ $collection->posts->comments->count() }}</span>
                                        <span class="view-count"><i class="icon icon-view"></i>{{ $collection->posts->views }}</span>
                                    </div><!-- .post-data -->
                                </a><!-- .post-link -->
                            </article><!-- .article-post -->
                        @endforeach
                        </div>
                        <!-- <p><i class="fa fa-flag-o flag"></i> {{ link_to_route('flag-user',t('Flag'),['username'=>$user->username],['class'=>'flag']) }}</p> -->
                    </div>

                    <div class="col-md-3"></div>                        
                </div>
            </div>
        </div>
    </div>
@stop

@section('pagination')
    {{ $posts->links() }}
@stop