@extends('master/index')
@section('meta_title')
    {{{ ucfirst($user->fullname) }}}'s {{ t('Collections') }} - {{ siteSettings('siteName') }}
@stop
@section('page-content')
    <div id="site-content" class="site-content user-view archives collections">
        @include('master/header')
        @include('user.partial.profileheader')

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        @include('user.partial.subnav')
                        @if(Auth::check())
                            @if(Auth::user()->username == $user->username)
                                <!-- <i class="fa fa-edit fa-fw"></i> {{ link_to_route('collection-create', t('Create new collection')) }} -->
                            @endif
                        @endif
                        <div class="articles-list">
                        @foreach($collections as $post)
                            <article id="" class="article-post">
                                <a href="{{ route('users-collection',['username'=>$post->user->username,'slug'=>$post->slug]) }}" title="{{ ucfirst(e($post->title))}}" class="post-link">
                                    <div class="post-info">
                                        <h3>{{ ucfirst(e($post->title))}}</h3>
                                        <div class="post-meta">
                                            <span class="post-date"><abbr class="timeago" title="{{ $post->created_at->toISO8601String() }}">{{ $post->created_at->toDateTimeString() }}</abbr></span>
                                            <span class="dot-sap"><i class="icon icon-dot"></i></span>
                                            <span class="post-author">{{ ucfirst($post->user->fullname) }}</span>
                                        </div><!-- .post-meta -->
                                        <p>{{ $post->summary }}</p>
                                    </div><!-- .post-info -->
                                    
                                    <div class="post-data">
                                        <span class="comments-count"><i class="icon icon-collections"></i>{{ $post->items->count() }}</span>
                                        <span class="view-count"><i class="icon icon-view"></i>{{ $post->views }}</span>
                                    </div><!-- .post-data -->
                                </a><!-- .post-link -->
                            </article><!-- .article-post -->
                        @endforeach
                        </div>
                    </div>

                    <div class="col-md-3"></div>                        
                </div>
            </div>
        </div>
    </div>


@stop

@section('pagination')
    {{ $collections->links() }}
@stop