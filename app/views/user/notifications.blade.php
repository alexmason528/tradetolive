@extends('master/index')

@section('page-content')
    <div id="site-content" class="site-content user-view">
        @include('master/header')
        @include('user.partial.profileheader')

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        @include('user.partial.subnav')
                        <div class="articles-list">
                        @foreach($notifications as $notice)
                        <article class="article-post">
                            @if($notice->post)
                                @if($notice->reason == 'comment')
                                    <p>{{ t('New comment on') }} {{ link_to_route($notice->post->type, ucfirst(e($notice->post->title)), ['id' => $notice->post->id, 'slug'=> $notice->post->slug], ['target'=>'_blank']) }} by {{ link_to_route('user',ucfirst(e($notice->fromUser->fullname)),['user' => $notice->fromUser->username]) }} <span class="pull-right"><abbr class="timeago" title="{{ $notice->created_at->toISO8601String() }}">{{ $notice->created_at->toDateTimeString() }}</abbr></span></p>
                                @endif

                                @if($notice->reason == 'reply')
                                    <p>{{ t('New reply on comment at') }} {{ link_to_route($notice->post->type, ucfirst(e($notice->post->title)), ['id' => $notice->post->id, 'slug'=> $notice->post->slug], ['target'=>'_blank']) }} by {{ link_to_route('user',ucfirst(e($notice->fromUser->fullname)),['user' => $notice->fromUser->username]) }} <span class="pull-right"><abbr class="timeago" title="{{ $notice->created_at->toISO8601String() }}">{{ $notice->created_at->toDateTimeString() }}</abbr></span></p>
                                @endif

                                @if($notice->reason == 'vote')
                                    <p>{{ t('New Vote on') }} {{ link_to_route($notice->post->type, ucfirst(e($notice->post->title)), ['id' => $notice->post->id, 'slug'=> $notice->post->slug], ['target'=>'_blank']) }} by {{ link_to_route('user',ucfirst(e($notice->fromUser->fullname)),['user' => $notice->fromUser->username]) }} <span class="pull-right"><abbr class="timeago" title="{{ $notice->created_at->toISO8601String() }}">{{ $notice->created_at->toDateTimeString() }}</abbr></span></p>
                                @endif
                            @endif
                            @if($notice->reason == 'follow')
                                <p>{{ t('You are now followed by') }} {{ link_to_route('user',ucfirst(e($notice->fromUser->fullname)),['user' => $notice->fromUser->username]) }}<span class="pull-right"><abbr class="timeago" title="{{ $notice->created_at->toISO8601String() }}">{{ $notice->created_at->toDateTimeString() }}</abbr></span></p>
                            @endif
                        </article>
                        @endforeach
                        </div>
                    </div>

                    <div class="col-md-3"></div>                        
                </div>
            </div>
        </div>
    </div>

    
@stop

@section('pagination')
    <div class="row">
        {{ $notifications->links() }}
    </div>

@stop