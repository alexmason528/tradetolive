@extends('master/index')
@section('meta_title')
    {{ t('Terms of Services') }} - {{ siteSettings('siteName') }}
@stop
@section('page-content')
    <div id="site-content" class="site-content policy">
        @include('master/header')
        <header id="site-header" class="site-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 header-left">  
                        <a class="back-button" href="{{ URL::previous() }}" title="Back"><i class="icon icon-arrow-left"></i>Back</a>                                          
                    </div><!-- .header-left -->

                    <div class="col-md-6 page-title">
                        <h1>{{ t('Terms of Services') }}</h1>
                    </div><!-- .page-title -->
                    
                    <div class="col-md-3 header-right"><!-- Blank column--></div><!-- .header-right -->
                </div><!-- .row -->
            </div><!-- .container-fluid -->                
        </header><!-- #site-header -->

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        
                        <p>{{ siteSettings('tos')}}</p>

                    </div>

                    <div class="col-md-3"></div>                        
                </div>
            </div>
        </div><!-- .page-content -->
    </div>
@stop