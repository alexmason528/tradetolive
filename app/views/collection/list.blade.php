@extends('master/index')
@section('meta_title')
    {{ strip_tags(ucfirst($title)) }} - {{ siteSettings('siteName') }}
@stop
@section('page-content')
    <div id="site-content" class="site-content archives collections">
        @include('master/header')
        <header id="site-header" class="site-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 header-left">
                        <a class="back-button" href="{{ URL::previous() }}" title="Back to listing"><i class="icon icon-arrow-left"></i>Back</a>
                    </div>

                    <div class="col-md-6 page-title">
                       <h1>{{ t('Collections') }}</h1>
                    </div>
                    
                    <div class="col-md-3 header-right"></div>
                </div>
            </div>
        </header>

        <aside id="sub-header" class="sub-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6 sub-content">
                        <ul class="tabs-lists">
                            <li><a href="#" class="active" title="Top Collections">Top Collections</a></li>
                        </ul><!-- .tabs-lists -->
                        @if(Auth::check())
                        <div class="sub-head-actions">                                
                            <a class="create-collection" href="{{ route('collection-create') }}" title="Create Collection"><i class="icon icon-collections"></i>Create</a>                                
                        </div><!-- .sub-head-actions -->
                        @endif
                    </div>
                    <div class="col-md-3"></div>                        
                </div><!-- .row -->
            </div><!-- .container-fluid -->
        </aside><!-- #sub-header -->

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="articles-list">
                            @foreach($collections as $post)
                            <article id="" class="article-post">
                                <a href="{{ route('users-collection',['username'=>$post->user->username,'slug'=>$post->slug]) }}" title="{{ ucfirst(e($post->title))}}" class="post-link">
                                    <div class="post-info">
                                        <h3>{{ ucfirst(e($post->title))}}</h3>
                                        <div class="post-meta">
                                            <span class="post-date"><abbr class="timeago" title="{{ $post->created_at->toISO8601String() }}">{{ $post->created_at->toDateTimeString() }}</abbr></span>
                                            <span class="dot-sap"><i class="icon icon-dot"></i></span>
                                            <span class="post-author">{{ ucfirst($post->user->fullname) }}</span>
                                        </div><!-- .post-meta -->
                                        <p>{{ $post->summary }}</p>
                                    </div><!-- .post-info -->
                                    
                                    <div class="post-data">
                                        <span class="comments-count"><i class="icon icon-collections"></i>{{ $post->items->count() }}</span>
                                        <span class="view-count"><i class="icon icon-view"></i>{{ $post->views }}</span>
                                    </div><!-- .post-data -->
                                </a><!-- .post-link -->
                            </article><!-- .article-post -->
                            @endforeach
                        </div>

                        <div class="pagination-wrapper">
                            <div class="align-right">{{ $collections->links() }}</p>
                        </div>
                        <!-- @foreach($collections as $post)
                            <div class="row post-item" id="{{ $post->id }}">
                                <div class="col-lg-10 col-md-9 col-sm-10 col-xs-10">
                                    <h2 class="post-title">{{ link_to_route('users-collection', ucfirst(e($post->title)), ['username' => $post->user->username, 'slug'=> $post->slug]) }}</h2>

                                    <p><abbr class="timeago" title="{{ $post->created_at->toISO8601String() }}">{{ $post->created_at->toDateTimeString() }}</abbr> in {{ t('by')}} {{ link_to_route('user',ucfirst($post->user->fullname), ['username'=>$post->user->username]) }}</p>
                                    <p>{{{ $post->summary }}}</p>

                                    <ul class="list-inline">
                                        <li><i class="fa fa-eye"></i></li>&nbsp;{{ $post->items->count() }}&nbsp;{{ t('items') }}
                                        <li><i class="fa fa-eye"></i>&nbsp;{{ $post->views }}&nbsp;{{ t('views') }}</li>
                                    </ul>
                                </div>
                             
                                <div class="col-lg-1 col-md-2 hidden-sm hidden-xs">
                                    <a href="{{ route('user',$post->user->username) }}"><img src="{{ getAvatar($post->user) }}" class="img-circle"/></a>
                                </div>
                             
                            </div>
                        @endforeach -->
                    </div>

                    <div class="col-md-3"></div>                        
                </div>
            </div>
        </div>
    </div>
@stop