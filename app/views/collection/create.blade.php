@extends('master/index')
@section('meta_title')
    {{ t('Create new collection') }}
@stop
@section('page-content')
    <div id="site-content" class="site-content archives collections">
        @include('master/header')
        <header id="site-header" class="site-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 header-left">
                        <a class="back-button" href="{{ URL::previous() }}" title="Back to listing"><i class="icon icon-arrow-left"></i>Back</a>
                    </div>

                    <div class="col-md-6 page-title">
                       <h1>{{ t('Create new collection') }}</h1>
                    </div>
                    
                    <div class="col-md-3 header-right"></div>
                </div>
            </div>
        </header>

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        {{ Form::open(array('class' => 'default-form')) }}
                        <div class="form-group">
                            <label for="title">{{ t('Title') }}*</label>
                            {{ Form::text('title',null,['placeholder' => t('Title'), 'required' => 'required']) }}
                        </div>

                        <div class="form-group">
                            <label for="title">{{ t('Summary') }}</label>
                            {{ Form::text('summary',null,['placeholder' => t('Summary'), 'required' => 'required']) }}
                        </div>

                        <div class="form-group">
                            <p class="align-left">{{ Form::submit(t('Submit'),['class'=>'submit-button']) }}</p>
                        </div>
                        {{ Form::close() }}
                    </div>
                    <div class="col-md-3"></div>                        
                </div>
            </div>
        </div>
    </div>
@stop