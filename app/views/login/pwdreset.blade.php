@extends('master/index')
@section('meta_title')
    {{ strip_tags('Password Reset') }} - {{ siteSettings('siteName') }}
@stop
@section('page-content')
    <div id="site-content" class="site-content login">
        @include('master/header')
        <header id="site-header" class="site-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 header-left">  
                        <a class="back-button" href="{{ URL::previous() }}" title="Back"><i class="icon icon-arrow-left"></i>Back</a>                               
                    </div>

                    <div class="col-md-6 page-title">
                        <h1>{{ t('Password Reset') }}</h1>
                    </div>
                    
                    <div class="col-md-3 header-right"></div>
                </div>
            </div>
        </header>

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        {{ Form::open(array('class'=>'default-form')) }}
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group">

                            <label for="email">{{ t('Email') }}<small>*</small></label>
                            {{ Form::text('email','',['id'=>'email','placeholder'=>'Your Email','required'=>'required']) }}

                        </div>
                        <div class="form-group">
                            <label for="password">{{ t('New Password') }}<small>*</small></label>
                            {{ Form::password('password',['id'=>'password','placeholder'=>'Enter Password','autocomplete'=>'off','required'=>'required']) }}
                        </div>

                        <div class="form-group">
                            <label for="password_confirmation">{{ t('Retype Confirmation') }}<small>*</small></label>
                            {{ Form::password('password_confirmation',['id'=>'password_confirmation','placeholder'=>'Confirm Password','autocomplete'=>'off','required'=>'required']) }}
                        </div>
                        {{ Form::submit('Reset Password',['class'=>'sutbmit-button'])}}
                        {{ Form::close() }}
                    </div>

                    <div class="col-md-3"></div>                        
                </div>
            </div>
        </div>
    </div>
@stop