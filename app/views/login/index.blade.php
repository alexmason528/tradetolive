@extends('master/index')
@section('meta_title')
    {{ strip_tags('Login') }} - {{ siteSettings('siteName') }}
@stop
@section('page-content')
    <div id="site-content" class="site-content login">
        @include('master/header')
        <header id="site-header" class="site-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 header-left">  
                        <a class="back-button" href="{{ URL::previous() }}" title="Back"><i class="icon icon-arrow-left"></i>Back</a>                       
                    </div>

                    <div class="col-md-6 page-title">
                        <h1>{{ t('Log in to trade2live') }}</h1>
                    </div>
                    
                    <div class="col-md-3 header-right"></div>
                </div>
            </div>
        </header>

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        
                        {{ Form::open(array('id' => 'loginForm', 'method' => 'post', 'class' => 'default-form')) }}

                        <div class="form-group">
                            <label for="loginEmail">{{ t('Username or Email') }}</label>
                            {{ Form::text('username','',['id'=>'loginEmail', 'placeholder' => t('Username or Email'), 'required'=>'required']) }}
                        </div>

                        <div class="form-group">
                            <label for="password">{{ t('Password') }}</label>
                            {{ Form::password('password',['id'=>'password','placeholder'=>t('Password'),'autocomplete'=>'off', 'required'=>'required']) }}
                        </div>


                        <div class="row submit-row">
                            <div class="col-xs-6">
                                <p><a href="{{ url('password/remind') }}" title="Forgot your password?">Forgot your password?</a></p>
                                <p><a href="{{ route('registration') }}" title="Register">Register now</a></p>
                            </div>

                            <div class="col-xs-6">
                                <p class="align-right">{{ Form::submit(t('Login'),['class'=>'submit-button']) }}</p>
                            </div>
                        </div>

                        {{ Form::close() }}

                    </div>

                    <div class="col-md-3"></div>                        
                </div>
            </div>
        </div>
    </div>
@stop