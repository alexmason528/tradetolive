@extends('master/index')
@section('meta_title')
    {{{ strip_tags($post->title) }}} - {{ siteSettings('siteName') }}
@stop
@section('meta_description')
    {{{ Str::limit(strip_tags($post->summary),200) }}}
@stop
@section('page-content')
    <div id="site-content" class="site-content post-view">
        @include('master/header')
        <header id="site-header" class="site-header" style="background-image: url({{ asset('new/images/post-background.jpg') }})">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 header-left"> 
                        <a class="back-button" href="{{ URL::previous() }}" title="Back to listing"><i class="icon icon-arrow-left"></i>Back</a>
                    </div>
                    <div class="col-md-6 page-title">
                        <div class="post-meta">
                            <span class="post-date">{{ $post->created_at->toDateTimeString() }}</span>
                            <span class="dot-sap"><i class="icon icon-dot"></i></span>
                            <span class="post-author">{{ ucfirst($post->user->fullname) }}</span>
                            <span class="shared"> shared:</span>
                        </div><!-- .post-meta -->

                        <h1>{{ ucfirst(e($post->title)) }}</h1>
                    </div><!-- .page-title -->
                    
                    <div class="col-md-3 header-right">
                        <ul class="header-right-actions">
                            <li><a href="{{ !Auth::user() ? route('login') : '#' }}" class="vote-btn {{ checkVoted($post->votes) ? 'voted' : '' }}" data-id="{{ $post->id }}"><i class="icon icon-arrow-up"></i><span id="data-number-{{ $post->id }}">{{ $post->votes->count() }}</span></a></li>

                            <li><a href="#" data-toggle="modal" data-target="#myModal" class = "flag"><i class="icon icon-collections"></i>Add</a></li>
                            <li><a href="#" title="Share"><i class="icon icon-share"></i>Share</a></li>
                        </ul>
                    </div><!-- .header-right -->


                </div><!-- .row -->
            </div><!-- .container-fluid -->                
        </header><!-- #site-header -->

        <aside id="sub-header" class="sub-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6 sub-content">
                        
                        <div class="post-meta">
                            @if($post->type == 'news')
                            <span class="post-ref"><i class="icon icon-arrow-right"></i><a href="{{ route('news-out', ['id' => $post->id, 'slug'=> $post->slug]) }}" target="_blank">{{$post->url}}</a></span>
                            <span class="dot-sap"><i class="icon icon-dot"></i></span>
                            @endif
                            <span class="post-tags">
                                <a href="{{ route('latest', ['category'=>$post->category->slug]) }}" title="Category {{ $post->category->name }}">{{ $post->category->name }}</a>
                            </span>
                        </div><!-- .post-meta -->
                        
                        <ul class="post-stats">
                            <li><a class="stat" href="#" title="Commnets">
                                <i class="icon icon-comments"></i>
                                <span>{{ $post->comments->count() }}</span></a>
                            </li>
                            <li><a class="stat" href="#" title="Views Count">
                                <i class="icon icon-view"></i>
                                <span>{{ $post->views }}</span></a>
                            </li>
                            <li><a class="stat" href="{{ route('flag', ['id'=>$post->id]) }}" title="Flag"><i class="icon icon-flag"></i></a></li>
                        </ul><!-- .sub-head-actions -->
                    </div>
                    <div class="col-md-3"></div>                        
                </div><!-- .row -->
            </div><!-- .container-fluid -->
        </aside><!-- #sub-header -->


        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        
                        <article class="post-contents">
                            <p>{{ $post->summary }}</p>
                            @if($post->type == 'news')
                            <div class="align-center">
                                <a href="{{ route('news-out', ['id' => $post->id, 'slug'=> $post->slug]) }}" title="Read More" target="_blank" rel="nofollow" class="primary-button">Go to the website</a>
                            </div>
                            @endif
                        </article><!-- .post-contents -->

                        @include('post/comments')

                    </div>
                    <div class="col-md-3"></div>                        
                </div>
            </div>
        </div><!-- .page-content -->

        @include('post/extra/related')




        <!-- <div class="row">
            <h2 class="page-header">{{{ $title }}}</h2>
            <div class="util-list">
                @if($previous)
                    <a href="{{ route($post->type, ['id' => $previous->id , 'slug' => $previous->slug]) }}"><span class="glyphicon glyphicon-chevron-left"></span>&nbsp;{{ t('Previous') }}</span></a>&nbsp;
                @endif
                @if($next)
                    <a href="{{ route($post->type, ['id' => $next->id , 'slug' => $next->slug]) }}">{{ t('Next') }} &nbsp;<span class="glyphicon glyphicon-chevron-right"></span></a>
                @endif
            </div>
        </div> -->
        
        @include('post/extra/collection-model')
    </div>
@stop