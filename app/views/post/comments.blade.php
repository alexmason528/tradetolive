<div class="comments">
    <div class="comment-write clearfix">
        <div class="user-image">
            <img src="{{ getAvatar($post->user, 64) }}" alt="{{ $post->user->fullname }}" width="60" height="60">
        </div><!-- .user-image -->

        <div class="comment-section">
            {{ Form::open(array('url' => route('post-comment', array('id' => $post->id, 'slug' => $post->slug )))) }}
                {{ Form::textarea('comment','',array('class'=>'expanding','placeholder' => 'Join the discussion...','rows'=>3)) }}
                {{ Form::submit(t('Add Comment'),array('class'=>'btn')) }}
            {{ Form::close() }}
        </div><!-- .comment-section -->
    </div><!-- .comment-write -->

    <ul class="comments-list">
        @foreach($comments as $comment)
            <li class="clearfix" id="comment-{{ $comment->id }}">
                <div class="author-image"> 
                    <img class="media-object img-circle" src="{{ getAvatar($comment->user,64) }}" alt="{{{ $comment->user->fullname }}}">
                </div><!-- .author-image -->
                <div class="comment-post">
                    @if(Auth::check())
                        @if($comment->user_id == Auth::user()->id || $post->user->id == Auth::user()->id)
                            <button data-content="{{ $comment->id }}" type="button" class="close delete-comment" aria-hidden="true">&times;</button>
                        @endif
                    @endif
                    <a href="{{ route('user',$comment->user->username) }}">
                        <h5 class="comment-author">{{{ ucfirst($comment->user->fullname) }}}</h5>
                    </a>
                    <div class="comment-meta"><i class="comment-time fa fa-clock-o"></i> <abbr class="timeago comment-time" title="{{ date(DATE_ISO8601,strtotime($comment->created_at)) }}">{{ date(DATE_ISO8601,strtotime($comment->created_at)) }}</abbr></div>
                    <div class="comment-text">
                        <p>{{ Smilies::parse(e($comment->description)) }}</p>
                    </div>
                    <span class="comment-vote"><a class="icon icon-arrow-up fa-fw {{ checkVoted($comment->votes) == true ? 'comment-voted':'' }}  {{ Auth::check() == true ? 'vote-comment':'' }}" data-id="{{ $comment->id }}"></a><span id="data-comment-{{ $comment->id }}">{{ $comment->votes->count() }}</span></span>
                    <div class="flag flag-disabled"><a href="#"><i class="icon icon-flag"></i></a></div>

                    @foreach($comment->reply as $reply)
                    <hr/>
                    <div class="media" id="reply-{{ $reply->id }}">
                        <a class="pull-left" href="{{ route('user',$reply->user->username) }}">
                            <img class="media-object img-circle" src="{{ getAvatar($reply->user,64) }}" alt="{{{ $reply->user->fullname }}}">
                        </a>

                        <div class="media-body">
                            <h4 class="media-heading"><a href="{{ url('user/'.$reply->user->username) }}">{{{ ucfirst($reply->user->fullname) }}}</a> <span class="pull-right">
                                @if(Auth::check())
                                    @if($reply->user_id == Auth::user()->id || $post->id == Auth::user()->id || $reply->comment->user->id == Auth::user()->id)
                                        <span class="right"><button data-content="{{ $reply->id }}" type="button" class="close delete-reply" aria-hidden="true">&times;</button></span>
                                    @endif
                                @endif
                                <i class="comment-time fa fa-clock-o"></i> <abbr class="timeago comment-time" title="{{ date(DATE_ISO8601,strtotime($reply->created_at)) }}">{{ date(DATE_ISO8601,strtotime($reply->created_at)) }}</abbr> </span></h4>
                            <p>{{ Smilies::parse(e($reply->description)) }}</p>

                            <p><span class="comment-vote"><span id="data-reply-{{ $reply->id }}">{{ $reply->votes->count() }}</span><a class="fa fa-chevron-circle-up fa-fw {{ checkVoted($reply->votes) == true ? 'comment-voted':'' }} {{ Auth::check() == true ? 'vote-reply':'' }}" data-id="{{ $reply->id }}"></a></span></p>
                        </div>
                    </div>
                    @endforeach

                </div><!-- .comment-post -->
            </li>
        @endforeach
        
    </ul><!-- .comments-list -->
</div><!-- .comments -->

<!-- <div class="row comments-block">
    @foreach($comments as $comment)
        <div class="media" id="comment-{{ $comment->id }}">
            <a class="pull-left" href="{{ route('user', $comment->user->username) }}">
                <img class="media-object img-circle" src="{{ getAvatar($comment->user,64) }}" alt="{{{ $comment->user->fullname }}}">
            </a>

            <div class="media-body">
                <h4 class="media-heading"><a href="{{ route('user',$comment->user->username) }}">{{{ ucfirst($comment->user->fullname) }}}</a> <span class="pull-right">
                         @if(Auth::check() == TRUE)
                            @if($comment->user_id == Auth::user()->id || $post->user->id == Auth::user()->id)
                                <button data-content="{{ $comment->id }}" type="button" class="close delete-comment" aria-hidden="true">&times;</button>@endif
                        @endif
                        <i class="comment-time fa fa-clock-o"></i> <abbr class="timeago comment-time" title="{{ date(DATE_ISO8601,strtotime($comment->created_at)) }}">{{ date(DATE_ISO8601,strtotime($comment->created_at)) }}</abbr> </span></h4>
                <p>{{ Smilies::parse(e($comment->description)) }}</p>
                <span class="comment-vote"><span id="data-comment-{{ $comment->id }}">{{ $comment->votes->count() }}</span> <a class="fa fa-chevron-circle-up fa-fw {{ checkVoted($comment->votes) == true ? 'comment-voted':'' }}  {{ Auth::check() == true ? 'vote-comment':'' }}" data-id="{{ $comment->id }}"></a></span>
                @if(Auth::check() == TRUE)
                    <a class="replybutton" id="box-{{ $comment->id }}">{{ t('Reply') }}</a>
                    <div class="commentReplyBox" id="openbox-{{ $comment->id }}">
                        <input type="hidden" name="pid" value="19">
                        {{ Form::textarea('comment','',array('id'=>'textboxcontent'.$comment->id,'class'=>"form-control",'rows'=>2,'placeholder'=>t('Comment'))) }}
                        </br>
                        <button class="btn btn-info replyMainButton" id="{{ $comment->id }}">{{ t('Reply') }}</button>
                        <a class="closebutton" id="box-{{ $comment->id }}">{{ t('Cancel') }}</a>
                    </div>
                    <span class="reply-add-{{ $comment->id }}"></span>
                    @endif
                    @foreach($comment->reply as $reply)
                        <hr/>
                        <div class="media" id="reply-{{ $reply->id }}">
                            <a class="pull-left" href="{{ route('user',$reply->user->username) }}">
                                <img class="media-object img-circle" src="{{ getAvatar($reply->user,64) }}" alt="{{{ $reply->user->fullname }}}">
                            </a>

                            <div class="media-body">
                                <h4 class="media-heading"><a href="{{ url('user/'.$reply->user->username) }}">{{{ ucfirst($reply->user->fullname) }}}</a> <span class="pull-right">
                        @if(Auth::check() == TRUE)
                                            @if($reply->user_id == Auth::user()->id || $post->id == Auth::user()->id || $reply->comment->user->id == Auth::user()->id)
                                                <span class="right"><button data-content="{{ $reply->id }}" type="button" class="close delete-reply" aria-hidden="true">&times;</button></span>
                                            @endif
                                        @endif
                                        <i class="comment-time fa fa-clock-o"></i> <abbr class="timeago comment-time" title="{{ date(DATE_ISO8601,strtotime($reply->created_at)) }}">{{ date(DATE_ISO8601,strtotime($reply->created_at)) }}</abbr> </span></h4>
                                <p>{{ Smilies::parse(e($reply->description)) }}</p>

                                <p><span class="comment-vote"><span id="data-reply-{{ $reply->id }}">{{ $reply->votes->count() }}</span> <a class="fa fa-chevron-circle-up fa-fw {{ checkVoted($reply->votes) == true ? 'comment-voted':'' }} {{ Auth::check() == true ? 'vote-reply':'' }}" data-id="{{ $reply->id }}"></a></span></p>
                            </div>
                        </div>
                        @endforeach
            </div>
            <hr/>
        </div>
    @endforeach
    {{ $comments->links() }}
</div> -->