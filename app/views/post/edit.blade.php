@extends('master/index')
@section('meta_title')
    {{{ $title }}} - {{{ $post->title }}}
@stop
@section('page-content')
    <div id="site-content" class="site-content archives">
        @include('master/header')
        <header id="site-header" class="site-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 header-left">
                        @include('master/util-list')
                    </div>

                    <div class="col-md-6 page-title">
                        <h1>{{{ $title }}}</h1>
                    </div><!-- .page-title -->
                    
                    <div class="col-md-3 header-right"><!-- Blank column--></div><!-- .header-right -->
                </div><!-- .row -->
            </div><!-- .container-fluid -->                
        </header><!-- #site-header -->

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">      
                        <div class="articles-list">
                            {{ Form::open(array('class'=>'default-form')) }}
                            @if($post->type == 'news')
                                <div class="form-group">
                                    <label for="url">{{ t('Link') }}*</label>
                                    {{ Form::text('url',$post->url,['placeholder' => t('Url of webpage'), 'required' => 'required']) }}
                                </div>
                            @endif

                            <div class="form-group">
                                <label for="title">{{ t('Title') }}*</label>
                                {{ Form::text('title',$post->title,['placeholder' => t('Title'), 'required' => 'required']) }}
                            </div>

                            <div class="form-group">
                                <label for="title">{{ t('Summary') }}</label>
                                {{ Form::textarea('summary',$post->summary,array('class' => 'ckeditor', 'id' => 'editor', 'placeholder' => t('Summary'))) }}
                            </div>

                            <div class="form-group">
                                <label for="category">{{ t('Category') }}*</label>
                                <select class="form-control" name="category" required>
                                    <option value="{{ $post->category->id }}">{{ $post->category->name }}</option>
                                    <option>-------</option>
                                    @foreach(siteCategories() as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="delete">{{ t('Delete') }}</label>
                                {{ Form::checkbox('delete',1) }}
                            </div>

                            <div class="form-group">
                                <p class="align-right">{{ Form::submit(t('Submit'),array('class'=>'button-submit')) }}</p>
                            </div>

                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop