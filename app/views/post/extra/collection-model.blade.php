<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{ t('Adding') }} "{{{ $post->title }}}" {{ t('to collection') }}</h4>
            </div>
            @if(Auth::check())
                <?php
                $collection = Auth::user()->collections;
                $items = Auth::user()->collectionItems()->with('collection')->wherePostId($post->id)->get();
                $inCollections = array_values($items->lists('collection_id'));
                ?>
                @if($collection->count() >= 1)
                    {{ Form::open(['url' => 'collection/addpost']) }}
                    <div class="modal-body">
                        <label for="id">{{ t('Select Collection') }}</label>
                        {{ Form::select('id', array_except(array_pluck($collection->toArray(), 'title', 'id'),$inCollections),1,['class' => 'form-control', 'required' => 'required']) }}
                        {{ Form::hidden('post_id', $post->id) }}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input type="submit" value="{{ t('Add to collection') }}" class="btn btn-primary"/>
                    </div>
                    {{ Form::close() }}
                @else
                    <div class="modal-body">
                        <p><strong>You don't have any collection <i class="fa fa-pencil"></i> <a href="{{ route('collection-create') }}">Click here to create new</a></strong></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                @endif

                @if($items->count() >= 1)
                    <div class="modal-body">
                        <p><strong>{{ t('This post is listed under these collections') }}</strong></p>
                        <ul class="list-group">
                            @foreach($items as $item)
                                @if($item->collection)
                                    <li class="list-group-item" id="collection-{{ $item->collection->id }}">
                                        <i class="fa fa-external-link fw"></i>
                                        <a href="{{ route('users-collection', ['username' => Auth::user()->username, 'slug' => $item->collection->slug]) }}">{{{ $item->collection->title }}}</a>

                                        <div class="pull-right action-buttons">
                                            <a href="#" class="text-danger delete-collection-item" data-content="{{ $item->collection->id }}" data-post="{{ $post->id }}"><span class="glyphicon glyphicon-trash"></span></a>
                                        </div>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                @endif
                <hr/>
                <div class="modal-body">
                    <p><strong><i class="fa fa-external-link fa-fw"></i><a href="{{ route('collection-create') }}">{{ t('Create new collection') }}</a></strong></p>
                </div>
            @else
                <div class="modal-body">
                    <p><strong><i class="fa fa-external-link fa-fw"></i><a href="{{ route('login') }}">{{ t('You need to login first') }}</a></strong></p>
                </div>
            @endif
        </div>
    </div>
</div>