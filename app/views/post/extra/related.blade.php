<div class="related-posts">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <h3>{{ t('Related Posts') }}</h3>
                <ul class="related-posts-list">
                    @foreach($related as $post)
                        <li>
                            <a href="{{ route($post->type,['id'=>$post->id,'slug'=>$post->slug]) }}" title="{{ $post->title }}">
                                <div class="user-image"> 
                                    <!-- <a href="{{ route('user',$post->user->username) }}"> --><img src="{{ getAvatar($post->user) }}" width="40" height="40" class="img-circle"/><!-- </a> -->
                                </div><!-- .author-image -->
                                <div class="post-info">
                                    <span class="post-author">{{ ucfirst($post->user->fullname) }} shared</span>
                                    <h4>{{ucfirst(e($post->title))}}</h4>          
                                </div><!-- .comment-post -->
                            </a>
                        </li>
                    @endforeach

                </ul>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</div><!-- .related-posts -->
<div class="clearfix"></div>