@extends('master/index')
@section('meta_title')
    {{ strip_tags(ucfirst($title)) }} - {{ siteSettings('siteName') }}
@stop
@section('page-content')

<div id="site-content" class="site-content archives">
    @include('master/header')
    <header id="site-header" class="site-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 header-left">
                    @include('master/util-list')
                </div>

                <div class="col-md-6 page-title">
                    @if(Input::get('category'))
                        <h1>{{{ ucfirst($title) }}} {{{ t('in category') }}} {{ getCategoryName(Input::get('category')) }}</h1>
                    @else
                        <h1>{{{ ucfirst($title) }}}</h1>
                    @endif
                </div><!-- .page-title -->
                
                <div class="col-md-3 header-right"><!-- Blank column--></div><!-- .header-right -->
            </div><!-- .row -->
        </div><!-- .container-fluid -->                
    </header><!-- #site-header -->

    <aside id="sub-header" class="sub-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 text-center">
                    <ul class="tabs-lists">
                        <li><a href="trending" {{ strpos(Request::url(), 'trending') ? ' class="active"' : null }} title="Trending">Trending</a></li>
                        <li><a href="latest" {{ strpos(Request::url(), 'latest') ? ' class="active"' : null }} title="Latest">Latest</a></li>
                        <li><a href="featured" {{ strpos(Request::url(), 'featured') ? ' class="active"' : null }} title="Featured">Featured</a></li>
                    </ul>
                </div>
                <div class="col-md-3"></div>                        
            </div><!-- .row -->
        </div><!-- .container-fluid -->
    </aside><!-- #sub-header -->

    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">      
                    <div class="articles-list">
                        @foreach($posts as $post)
                            <article id="{{ $post->id }}" class="article-post">
                                <a href="{{ route($post->type,['id'=>$post->id,'slug'=>$post->slug]) }}" title="{{ ucfirst(e($post->title))}}" class="post-link">
                                    <div class="up-count">
                                        <i class="icon icon-arrow-up"></i>{{ $post->votes->count() }}
                                    </div><!-- .up-count -->

                                    <div class="post-info">
                                        <div class="post-info-wrapper">
                                            <cite>{{ parseUrl($post->url) }}</cite>
                                            <h3>{{ ucfirst(e($post->title)) }}</h3>
                                            <div class="post-meta">
                                                <span class="post-date"><abbr class="timeago" title="{{ $post->approved_at->toISO8601String() }}">{{ $post->approved_at->toDateTimeString() }}</abbr></span>
                                                <span class="post-catgories"><span>{{ $post->category->name }}</span></span>
                                                <span class="post-author">{{ ucfirst($post->user->fullname) }}</span>
                                            </div><!-- .post-meta -->
                                        </div>
                                    </div><!-- .post-info -->
                                    
                                    <div class="post-data">
                                        <span class="comments-count"><i class="icon icon-comments"></i>{{ $post->comments->count() }}</span>
                                        <span class="view-count"><i class="icon icon-view"></i>{{ $post->views }}</span>
                                    </div><!-- .post-data -->
                                </a><!-- .post-link -->
                            </article><!-- .article-post -->
                        @endforeach
                    </div>
                    <div class="pagination-wrapper">
                        <div class="align-right">{{ $posts->links() }}</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@stop