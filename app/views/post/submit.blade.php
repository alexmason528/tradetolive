@extends('master/index')
@section('meta_title')
    {{ t('Submitting News') }}
@stop
@section('page-content')
    <div id="site-content" class="site-content login">
        @include('master/header')
        <header id="site-header" class="site-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 header-left">  
                        <a class="back-button" href="{{ URL::previous() }}" title="Back"><i class="icon icon-arrow-left"></i>Back</a>                               
                    </div>

                    <div class="col-md-6 page-title">
                        <h1>{{ t('Add New Post') }}</h1>
                    </div>
                    
                    <div class="col-md-3 header-right"></div>
                </div>
            </div>
        </header>

        <aside id="sub-header" class="sub-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6 sub-content">
                        <ul class="link-list">
                            <li><a href="{{ route('news-submit') }}" {{ strpos(Request::url(), 'news/submit') ? ' class="active"' : null }} title="Submit an Article">Submit an Article</a></li>
                            <li><a href="{{ route('question-submit') }}" {{ strpos(Request::url(), 'question/submit') ? ' class="active"' : null }} title="Ask a Question">Ask a Question</a></li>
                        </ul><!-- .tabs-lists -->                            
                    </div>
                    <div class="col-md-3"></div>                        
                </div><!-- .row -->
            </div><!-- .container-fluid -->
        </aside><!-- #sub-header -->

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        {{ Form::open(array('class'=>'default-form')) }}
                        <div class="form-group">
                            <label for="url">{{ t('Link') }}*</label>
                            {{ Form::text('url',null,['placeholder' => t('Url of webpage'), 'required'=>'required']) }}
                        </div>

                        <div class="form-group">
                            <label for="title">{{ t('Title') }}*</label>
                            {{ Form::text('title',null,['placeholder' => t('Title'), 'required'=>'required']) }}
                        </div>

                        <div class="form-group">
                            <label for="title">{{ t('Summary') }}</label>
                            {{ Form::textarea('summary',null,['class' => 'ckeditor', 'id' => 'editor', 'placeholder' => t('Summary'), 'required'=>'required']) }}
                        </div>

                        <div class="form-group">
                            <label for="category">{{ t('Category') }}*</label>
                            <select class="form-control" name="category">
                                <option>-------</option>
                                @foreach(siteCategories() as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <p class="align-center">{{ Form::submit(t('Submit'),['class'=>'submit-button']) }}</p>
                        </div>

                        {{ Form::close() }}
                    </div>

                    <div class="col-md-3"></div>                        
                </div>
            </div>
        </div>
    </div>
@stop