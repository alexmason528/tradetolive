@extends('master/index')
@section('meta_title')
    {{ strip_tags(ucfirst($title)) }} - {{ siteSettings('siteName') }}
@stop
@section('page-content')
    <div id="site-content" class="site-content login">
        @include('master/header')
        <header id="site-header" class="site-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 header-left">
                    </div>

                    <div class="col-md-6 page-title">
                        @if(Input::get('category'))
                            <h1>{{{ ucfirst($title) }}} {{{ t('in category') }}} {{ getCategoryName(Input::get('category')) }}</h1>
                        @else
                            <h1>{{{ ucfirst($title) }}}</h1>
                        @endif
                    </div>
                    
                    <div class="col-md-3 header-right"></div>
                </div>
            </div>
        </header>

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="articles-list">
                        @foreach($posts as $key => $pts)
                            <div class="archive-heading">
                                @if($date = \Carbon\Carbon::createFromFormat('d-M-y',$key)->isToday())
                                    Today
                                @elseif($date = \Carbon\Carbon::createFromFormat('d-M-y',$key)->isYesterday())
                                    Yesterday
                                @else
                                    {{ date('l', strtotime($key))  }}
                                @endif
                                <small>{{ $key }}</small>
                            </div>
                            @foreach($pts as $post)
                            <article id="" class="article-post">
                                <a href="{{ route($post->type,['id'=>$post->id,'slug'=>$post->slug]) }}" title="{{ ucfirst(e($post->title))}}" class="post-link">
                                    <div class="up-count">
                                        <i class="icon icon-arrow-up"></i>{{ $post->votes->count() }}
                                    </div><!-- .up-count -->

                                    <div class="post-info">
                                        <div class="post-info-wrapper">
                                            <cite>{{ parseUrl($post->url) }}</cite>
                                            <h3>{{ ucfirst(e($post->title)) }}</h3>
                                            <div class="post-meta">
                                                <span class="post-date"><abbr class="timeago" title="{{ $post->created_at->toISO8601String() }}">{{ $post->created_at->toDateTimeString() }}</abbr></span>
                                                <span class="post-catgories">{{ $post->category->name }}</span>
                                                <span class="post-author">{{ $post->user->fullname }}</span>
                                            </div><!-- .post-meta -->
                                        </div>
                                    </div><!-- .post-info -->
                                    
                                    <div class="post-data">
                                        <span class="comments-count"><i class="icon icon-comments"></i>{{ $post->comments->count() }}</span>
                                        <span class="view-count"><i class="icon icon-view"></i>{{ $post->views }}</span>
                                    </div><!-- .post-data -->
                                </a><!-- .post-link -->
                            </article><!-- .article-post -->
                            @endforeach
                        @endforeach
                        </div>
                        <div class="pagination-wrapper">
                            <div class="align-right">{{ $pagination->links() }}</p>
                        </div>
                    </div>

                    <div class="col-md-3"></div>                        
                </div>
            </div>
        </div>
    </div>
@stop