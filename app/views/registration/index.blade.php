@extends('master/index')
@section('meta_title')
    {{ strip_tags('Registration') }} - {{ siteSettings('siteName') }}
@stop
@section('page-content')
    <div id="site-content" class="site-content login">
        @include('master/header')
        <header id="site-header" class="site-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 header-left">  
                        <a class="back-button" href="{{ URL::previous() }}" title="Back"><i class="icon icon-arrow-left"></i>Back</a>                               
                    </div>

                    <div class="col-md-6 page-title">
                        <h1>{{ t('Registration') }}</h1>
                    </div>
                    
                    <div class="col-md-3 header-right"></div>
                </div>
            </div>
        </header>

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        @if (Session::has('error'))
                            <div class="alert alert-danger fade in">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <strong>{{ trans(Session::get('reason')) }}</strong>
                            </div>
                        @endif
                        <div class="row">
                            {{ Form::open(array('class'=>'default-form')) }}
                            <div class="form-group">
                                <label for="username">{{ t('Select Username') }}
                                    <small>*</small>
                                </label>
                                {{ Form::text('username','',['id'=>'username','placeholder'=>t('Select Username'),'required'=>'required'])}}
                            </div>
                            <div class="form-group">
                                <label for="email">{{ t('Your Email') }}
                                    <small>*</small>
                                </label>
                                {{ Form::text('email','',['type'=>'email','id'=>'email','placeholder'=>t('Your Email'),'required'=>'required'])}}
                            </div>
                            <div class="form-group">
                                <label for="fullname">{{ t('Your Full Name') }}
                                    <small>*</small>
                                </label>
                                {{ Form::text('fullname','',['id'=>'fullname','placeholder'=>t('Your Full Name'),'required'=>'required'])}}
                            </div>

                            <div class="form-group">
                                <label for="gender">{{ t('Gender') }}
                                    <small>*</small>
                                </label>
                                {{ Form::select('gender', ['male' => 'Male', 'female' => 'Female'], 'male',['id'=>'gender','class'=>'form-control', 'required'=>'required']) }}
                            </div>


                            <div class="form-group">
                                <label for="password">{{ t('Password') }}
                                    <small>*</small>
                                </label>
                                {{ Form::password('password',['id'=>'password','placeholder'=>t('Enter Password'),'autocomplete'=>'off','required'=>'required']) }}
                            </div>
                            <div class="form-group">
                                <label for="password_confirmation">{{ t('Retype Password') }}
                                    <small>*</small>
                                </label>
                                {{ Form::password('password_confirmation',['id'=>'password_confirmation','placeholder'=>'Confirm Password','autocomplete'=>'off','required'=>'required']) }}
                            </div>
                            <div class="form-group">
                                <label for="recaptcha">{{ t('Type these words') }}
                                    <small>*</small>
                                </label>
                                {{ app('captcha')->display() }}
                            </div>
                            <div class="row submit-row">
                                <div class="col-xs-6">
                                    <p><a href="{{ url('password/remind') }}" title="Forgot your password?">Forgot your password?</a></p>
                                    <p><a href="{{ route('login') }}" title="Login">Already have account? Log In</a></p>
                                </div>

                                <div class="col-xs-6">
                                    <p class="align-right">{{ Form::submit(t('Create New Account'),['class'=>'submit-button'])}}</p>
                                </div>
                            </div><!-- .submit-row -->

                            <div class="form-notes">
                                <p>Creating an account means you’re okay with Trade2Live's <a href="{{ route('tos') }}" title="Terms of Service">Terms of Service</a> and <a href="{{ route('tos') }}" title="Privacy Policy">Privacy Policy</a>. <br>
                                Questions? <a href="mailto:support@trade2live.com">support@trade2live.com</a></p>
                            </div><!-- .form-notes -->
                            
                            {{ Form::close() }}
                        </div>
                    </div>

                    <div class="col-md-3"></div>                        
                </div>
            </div>
        </div>
    </div>
@stop