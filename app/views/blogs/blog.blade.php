@extends('master/index')

@section('page-content')
	<div id="site-content" class="site-content archives collections">
        @include('master/header')
        <header id="site-header" class="site-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 header-left">
                        <a class="back-button" href="{{ URL::previous() }}" title="Back to listing"><i class="icon icon-arrow-left"></i>Back</a>
                    </div>

                    <div class="col-md-6 page-title">
                       <h1>{{ link_to_route('blog', ucfirst($post->title), ['id' => $post->id, 'slug' => $post->slug]) }}</h1>
                    </div>
                    
                    <div class="col-md-3 header-right"></div>
                </div>
            </div>
        </header>

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
						<p>{{ $post->created_at->diffForHumans() }} {{ t('by')}} {{ link_to_route('user',ucfirst(e($post->user->fullname)), ['username'=>$post->user->username]) }}</p>
				        <p>{{ $post->description }}</p>
				        @include('post/extra/social-share')
                    </div>
                    <div class="col-md-3"></div>                        
                </div>
            </div>
        </div>
    </div>
@stop