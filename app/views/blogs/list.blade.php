@extends('master/index')

@section('page-content')
    <div id="site-content" class="site-content archives collections">
        @include('master/header')
        <header id="site-header" class="site-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 header-left">
                        <a class="back-button" href="{{ URL::previous() }}" title="Back to listing"><i class="icon icon-arrow-left"></i>Back</a>
                    </div>

                    <div class="col-md-6 page-title">
                       <h1>{{ ucfirst($title) }}</h1>
                    </div>
                    
                    <div class="col-md-3 header-right"></div>
                </div>
            </div>
        </header>

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        @foreach($blogs as $blog)
                            <div class="row post-item">
                                <h2 class="post-title">{{ link_to_route('blog', ucfirst($blog->title), ['id' => $blog->id, 'slug' => $blog->slug]) }}</h2>

                                <p>{{ $blog->created_at->diffForHumans() }} {{ t('by')}} {{ link_to_route('user',ucfirst(e($blog->user->fullname)), ['username'=>$blog->user->username]) }}</p>

                                <p>{{ $blog->description }}</p>
                                <hr/>
                            </div>
                        @endforeach
                    </div>
                    <div class="col-md-3"></div>                        
                </div>
            </div>
        </div>
    </div>
@stop

@section('pagination')
    {{ $blogs->links() }}
@stop