<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('meta_title', siteSettings('siteName'))</title>
    <link rel="shortcut icon" href="{{ asset(siteSettings('favIcon')) }}" type="image/x-icon"/>
    <meta name="description" content="@yield('meta_description', siteSettings('siteName'))"/>
    <meta property="og:title" content="@yield('meta_title', siteSettings('siteName'))"/>
    <meta property="og:type" content="website"/>
    <meta property='og:description' content="@yield('meta_description', siteSettings('siteName'))"/>
    <meta property="og:url" content="@yield('og_url',Request::url())"/>
    <meta property="og:site_name" content="{{ siteSettings('siteName') }}"/>
    <!--[if IE 8]>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- {{ HTML::style('static/css/sb-admin.css') }} -->

    


    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- Stylesheets: [Fonts, Bootstrap, style.css ] 
            <link href='css/bootstrap.min.css' rel='stylesheet' type='text/css'>
    -->

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>        
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        

    <!-- javascript: [jQuery, Bootstrap, script.js ] -->
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

    {{ HTML::style('static/font-awesome/css/font-awesome.css') }}
    {{ HTML::style('static/css/rrssb.css') }}
    {{ HTML::style('static/css/oembed.css') }}
    {{ HTML::style('static/css/nprogress.css') }}
    {{ HTML::style('new/css/style.css') }}   
    

</head>

<body>

<div id="wrapper">
    @include('master/notices')
    @include('master/navbar')
    @yield('page-content')
</div>
<!-- /#wrapper -->

<!-- Core Scripts - Include with every page -->
<!--{{ HTML::script('static/js/jquery-1.10.2.js') }}
{{ HTML::script('static/js/bootstrap.min.js') }}-->
{{ HTML::script('static/js/plugins/metisMenu/jquery.metisMenu.js') }}
{{ HTML::script('static/js/jquery.timeago.js') }}
{{ HTML::script('static/js/rrssb.min.js') }}
{{ HTML::script('static/js/json2html.js') }}
{{ HTML::script('static/js/jquery.json2html.js') }}
{{ HTML::script('static/js/jquery.oembed.js') }}
{{ HTML::script('static/js/nprogress.js') }}
{{ HTML::script('static/js/editor/ckeditor.js') }}
{{ HTML::script('static/js/custom.js') }}
{{ HTML::script('new/js/script.js') }}    

</body>
</html>