<div class="mobile-header visible-xs">
    <div class="menu-toggle">
        <a href="#" id="menuToggle" class="menu-toggle-icon">
            <span></span></a>
    </div><!-- .menu-toggle -->
    <div class="account-link">
        <div id="mobile-account-dropdown" class="dropdown list-dropdown mobile-list-dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="mobAccountDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <img src="{{ asset('new/images/login.png') }}" alt="user" width="30px" height="30px">
            </button>
            @if(Auth::check())
            <ul class="dropdown-menu" aria-labelledby="accountDropdown">
                <li><a href="{{ route('user-post', ['username' => Auth::user()->username]) }}"><i class="fa fa-user fa-fw"></i> {{ t('My Profile')}}</a>
                </li>
                <li><a href="{{ route('users-collection', ['username' => Auth::user()->username]) }}"><i class="fa fa-th-large fa-fw"></i> {{ t('My Collections') }}</a>
                </li>
                <li><a href="{{ route('settings')}}"><i class="fa fa-gear fa-fw"></i> {{ t('Settings') }}</a>
                </li>
                <li class="divider"></li>
                <li><a href="{{ route('logout')}}"><i class="fa fa-sign-out fa-fw"></i> {{ t('Logout')}}</a>
                </li>
            </ul>
            @else
            <ul class="dropdown-menu" aria-labelledby="mobAccountDropdown">
                <li>{{ link_to_route('login',t('Login')) }}</li>
                <li>{{ link_to_route('registration',t('Create Account')) }}</li>                            
            </ul>
            @endif
        </div><!-- .list-dropdown -->                                                        
    </div><!-- .account-link -->
    <div class="mobile-logo">
        <a href="/" title="Trade2live"><img src="{{ asset('new/images/trade2live.svg') }}" width="" alt="Trade2live"></a>
    </div><!-- .mobile-logo -->
</div><!-- .mobile-header :only visible in mobile -->

<div class="mobile-new-post visible-xs">
    <a href="{{ route('news-submit') }}" title="New Post" class="new-post-link"><i class="icon icon-new-post"></i></a>
</div><!-- .mobile-new-post -->

<aside id="top-bar" class="top-bar">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                {{ Form::open(['url'=>'search', 'method' => 'get', 'id' => 'searchForm', 'class' => 'search-form']) }}
                <p class="search-icon"><input type="text" name="q" class="search-form-input" placeholder="{{ t('Search') }}..."></p>
                {{ Form::close() }}
            </div>
            <div class="col-sm-6 top-nav">
                <div class="account-info">
                @if(Auth::check())
                <a href="{{ route('users-feeds') }}" title="feeds"><i class="fa fa fa-bolt fa-fw"></i></a>
                <a href="{{ route('notifications') }}" title="notification"><i class="fa fa fa-bell fa-fw"></i> <span class="badge">{{ (Auth::user()->notifications()->whereNull('seen_at')->count() > 0  ? Auth::user()->notifications()->whereNull('seen_at')->count() :false) }}</span></a>
                <div id="account-dropdown" class="dropdown list-dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="accountDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <i class="fa fa-user fa-fw"></i><span class="icon icon-arrow-down"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="accountDropdown">
                        <li><a href="{{ route('user-post', ['username' => Auth::user()->username]) }}"><i class="fa fa-user fa-fw"></i> {{ t('My Profile')}}</a>
                        </li>
                        <li><a href="{{ route('users-collection', ['username' => Auth::user()->username]) }}"><i class="fa fa-th-large fa-fw"></i> {{ t('My Collections') }}</a>
                        </li>
                        <li><a href="{{ route('settings')}}"><i class="fa fa-gear fa-fw"></i> {{ t('Settings') }}</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ route('logout')}}"><i class="fa fa-sign-out fa-fw"></i> {{ t('Logout')}}</a>
                        </li>
                    </ul>
                </div><!-- .list-dropdown -->
                @else
                    {{ link_to_route('login',t('Login')) }}
                    {{ link_to_route('registration',t('Create Account')) }}
                @endif
                </div><!-- .account-info -->
                <a href="{{ route('news-submit') }}" title="New Post" class="new-post-link"><i class="icon icon-new-post"></i>{{ t('Submit') }}</a>
            </div><!-- .top-nav -->
        </div>
    </div>
</aside><!-- #top-bar -->