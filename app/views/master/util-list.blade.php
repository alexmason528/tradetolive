<div id="categories-dropdown" class="dropdown list-dropdown">
    <button class="btn btn-default dropdown-toggle" type="button" id="catDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        @if(Input::get('category'))
            Category Filter : {{{ getCategoryName(Input::get('category')) }}}
        @else
            Select Category
        @endif
        <span class="icon icon-arrow-down"></span>
    </button>
    <ul class="dropdown-menu" aria-labelledby="catDropdown">
        @foreach(siteCategories() as $category)
            <li><a href="{{ Request::url() }}?{{ query_params(['category' => $category->slug]) }}">{{ $category->name }}</a></li>
        @endforeach
        @if(Input::get('category'))
            <li><a href="{{ Request::url() }}">All</a></li>
        @endif
    </ul>
</div>

<div id="days-dropdown" class="dropdown list-dropdown">
    <button class="btn btn-default dropdown-toggle" type="button" id="daysDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        @if(Input::get('timeframe') && in_array(Input::get('timeframe'),['now','week','month','year']))
            Time : {{{ t(ucfirst(Input::get('timeframe'))) }}}
        @else
            Time Frame
        @endif
        <span class="icon icon-arrow-down"></span>
    </button>
    <ul class="dropdown-menu" aria-labelledby="daysDropdown">
        <li><a href="{{ Request::url() }}?{{ query_params(['timeframe' => 'now']) }}">{{ t('Now') }}</a></li>
        <li><a href="{{ Request::url() }}?{{ query_params(['timeframe' => 'week']) }}">{{ t('Week') }}</a></li>
        <li><a href="{{ Request::url() }}?{{ query_params(['timeframe' => 'month']) }}">{{ t('Month')}}</a></li>
        <li><a href="{{ Request::url() }}?{{ query_params(['timeframe' => 'year']) }}">{{ t('Year') }}</a></li>
        @if(Input::get('timeframe'))
            <li><a href="{{ Request::url() }}">All</a></li>
        @endif                                 
    </ul>
</div>