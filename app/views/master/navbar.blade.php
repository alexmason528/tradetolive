<aside id="navigation-bar" class="left-bar">                
    <header class="branding">
        <a href="{{ url() }}" title="TRADE2LIVE"><img src="{{ asset('new/images/trade2live.svg') }}" alt="Trade2live"></a>
    </header><!-- .branding -->

    <nav class="navigation">
        <ul class="nav">
            <li><a href="{{ route('trending') }}" {{ Request::is('trending') ? ' class="active"' : null }}><i class="icon icon-trending"></i>{{ t('Trending') }}</a></li>
            <li><a href="{{ route('latest') }}" {{ Request::is('latest') ? ' class="active"' : null }}><i class="icon icon-latest"></i>{{ t('Latest') }}</a></li>
            <li><a href="{{ route('featured') }}" {{ Request::is('featured') ? ' class="active"' : null }}><i class="icon icon-featured"></i>{{ t('Featured') }}</a></li>
            <li><a href="{{ route('news-latest') }}" {{ Request::is('news/latest') || Request::is('news/trending') || Request::is('news/featured') || strpos(Request::url(), 'news/') ? ' class="active"' : null }}><i class="icon icon-news"></i>{{ t('News') }}</a></li>
            <li><a href="{{ route('questions-latest') }}" {{ Request::is('questions/latest') || Request::is('questions/trending') || Request::is('questions/featured') || strpos(Request::url(), 'question/') ? ' class="active"' : null }}><i class="icon icon-questions"></i>{{ t('Questions') }}</a></li>
            <li><a href="{{ route('collections') }}" {{ Request::is('collections') ? ' class="active"' : null }}><i class="icon icon-collections"></i>{{ t('Collections') }}</a></li>
            <li><a href="{{ route('archive') }}" {{ Request::is('archive') ? ' class="active"' : null }}><i class="icon icon-archive"></i>{{ t('Archive') }}</a></li>
            <li><a href="{{ route('news-submit') }}" {{ Request::is('news-submit') ? ' class="active"' : null }}><i class="icon icon-new-post"></i>{{ t('Submit') }}</a></li>
        </ul>
    </nav><!-- .navigation -->

    <footer class="site-footer">
        <div class="social-wrapper">
            <ul class="social-profiles">
                <li><a href="http://www.twitter.com/{handle}" title="Trade2live on Twitter" target="_blank"><img src="{{ asset('new/images/svg/twitter.svg') }}" alt="Twitter"></a></li>
                <li><a href="http://www.linkedin.com/{profile}" title="Trade2live on LinkedIn" target="_blank"><img src="{{ asset('new/images/svg/linkedin.svg') }}" alt="Linkedin"></a></li>
                <li><a href="http://www.facebook.com/{name}" title="Trade2live on Facebook" target="_blank"><img src="{{ asset('new/images/svg/facebook.svg') }}" alt="Facebook"></a></li>
            </ul>
        </div><!-- .social-wrapper -->

        <ul class="site-links">
            <li><a href="{{ route('about') }}" title="About Us">About Us</a></li>
            <li><a href="{{ route('privacy') }}" title="Privacy Policy">Privacy Policy</a></li>
            <li><a href="{{ route('tos') }}" title="Terms">Terms</a></li>
        </ul><!-- .site-links -->         
    </footer><!-- .site-footer -->
</aside><!-- #navigation-bar -->