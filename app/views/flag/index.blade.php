@extends('master/index')

@section('page-content')
<div id="site-content" class="site-content archives collections">
    @include('master/header')
    <header id="site-header" class="site-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 header-left">
                    <a class="back-button" href="{{ URL::previous() }}" title="Back to listing"><i class="icon icon-arrow-left"></i>Back</a>
                </div>

                <div class="col-md-6 page-title">
                   <h1>{{ t('Flagging Post') }}</h1>
                </div>
                
                <div class="col-md-3 header-right"></div>
            </div>
        </div>
    </header>

    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    {{ Form::open(array('class'=>'default-form')) }}
                    <div class="form-group">
                        <label for="title">{{ t('Reason') }}</label>
                        {{ Form::textarea('reason','',array('placeholder' => t('Reason'), 'required'=>'required')) }}
                    </div>

                    <div class="form-group">
                        {{ Form::submit(t('Submit'),array('class'=>'submit-button')) }}
                    </div>

                    {{ Form::close() }}
                </div>

                <div class="col-md-3"></div>                        
            </div>
        </div>
    </div>
</div>
@stop