<?php
/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
Route::resource('install', 'InstallController');

// Patterns
Route::pattern('id', '[0-9]+');

// Browsing Routes
Route::get('/', ['as' => 'home', 'uses' => 'BrowseController@getTrending']);
Route::get('trending', ['as' => 'trending', 'uses' => 'BrowseController@getTrending']);
Route::get('latest', ['as' => 'latest', 'uses' => 'BrowseController@getLatest']);
Route::get('featured', ['as' => 'featured', 'uses' => 'BrowseController@getFeatured']);
Route::get('archive', ['as' => 'archive', 'uses' => 'BrowseController@getArchive']);
Route::get('collections', ['as' => 'collections', 'uses' => 'CollectionController@getPopularCollection']);
// News Routes
Route::get('news/trending', ['as' => 'news-trending', 'uses' => 'NewsController@getTrending']);
Route::get('news/latest', ['as' => 'news-latest', 'uses' => 'NewsController@getLatest']);
Route::get('news/featured', ['as' => 'news-featured', 'uses' => 'NewsController@getFeatured']);

// Questions Routes
Route::get('questions/trending', ['as' => 'questions-trending', 'uses' => 'QuestionsController@getTrending']);
Route::get('questions/latest', ['as' => 'questions-latest', 'uses' => 'QuestionsController@getLatest']);
Route::get('questions/featured', ['as' => 'questions-featured', 'uses' => 'QuestionsController@getFeatured']);

Route::get('rss', ['as' => 'rss', 'uses' => 'RssController@getRss']);
/**
 * Guest only visit this section
 */
Route::group(['before' => 'guest'], function ()
{
    Route::get('login', ['as' => 'login', 'uses' => 'LoginController@index']);
    // Facebook
    Route::get('get/facebook', 'LoginController@loginWithFacebook');
    Route::get('registration/facebook', 'RegistrationController@getFacebook');
    // Google
    Route::get("get/google", 'LoginController@loginWithGoogle');
    Route::get("registration/google", 'RegistrationController@getGoogle');

    Route::controller('password', 'RemindersController');
    Route::get('registration', ['as' => 'registration', 'uses' => 'RegistrationController@getIndex']);
    Route::get('registration/activate/{username}/{code}', 'RegistrationController@validateUser');
});
/**
 * Guest routes require csrf protection
 */
Route::group(['before' => 'guest|csrf'], function ()
{
    Route::post('login', 'LoginController@postLogin');
    // Facebook
    Route::post('registration/facebook', 'RegistrationController@postFacebook');
    // Google
    Route::post('registration/google', 'RegistrationController@postGoogle');
    // Normal Registration
    Route::post('registration', 'RegistrationController@postIndex');
    Route::post('password/remind', 'PasswordresetController@postIndex');
    Route::post('password/reset/{token}', 'PasswordresetController@resetPassword');
});

// Group routes ( Require Login )
Route::group(['before' => 'auth'], function ()
{
    Route::get('feeds', ['as' => 'users-feeds', 'uses' => 'UsersController@getFeeds']);
    Route::get('news/submit', ['as' => 'news-submit', 'uses' => 'NewsController@index'])->before('ban');
    Route::post('news/submit', 'NewsController@create')->before('csrf|ban');
    Route::post('vote', 'NewsController@vote');

    Route::get('collection/create', ['as' => 'collection-create', 'uses' => 'CollectionController@getCreate']);
    Route::post('collection/create', 'CollectionController@postCreate')->before('csrf');
    Route::post('collection/addpost', 'CollectionController@addPost')->before('csrf');
    Route::post('collection/itemremove', 'CollectionController@removeItem');

    Route::get('question/submit', ['as' => 'question-submit', 'uses' => 'QuestionsController@index'])->before('ban');
    Route::post('question/submit', 'QuestionsController@create')->before('csrf|ban');

    Route::post('comment/{id}-{slug?}', ['as' => 'post-comment', 'uses' => 'CommentsController@create'])->before('csrf|ban');
    Route::post('reply', 'ReplyController@create');
    //Settings
    Route::get('settings', ['as' => 'settings', 'uses' => 'UsersController@getSettings']);
    Route::post('settings/profile', 'UsersController@updateProfile')->before('csrf');
    Route::post('settings/email', 'UsersController@updateProfile')->before('csrf');
    Route::post('settings/mailsettings', 'UsersController@mailSettings')->before('csrf');
    Route::post('settings/changepassword', 'UsersController@changePassword')->before('csrf');
    Route::post('settings/avatar', 'UsersController@updateAvatar')->before('csrf');

    // Logout
    Route::get('logout', ['as' => 'logout', 'uses' => 'LoginController@destroy']);

    // Flags Route
    Route::get('flag/{id}', ['as' => 'flag', 'uses' => 'FlagsController@getIndex']);
    Route::post('flag/{id}', 'FlagsController@postReport')->before('csrf|ban');
    Route::get('flag/user/{username}', ['as' => 'flag-user', 'uses' => 'FlagsController@getUserIndex']);
    Route::post('flag/user/{username}', 'FlagsController@postUserReport')->before('csrf|ban');

    // Notification
    Route::get('notifications', ['as' => 'notifications', 'uses' => 'UsersController@getNotifications']);
});

// News
Route::get('news', 'NewsController@getLatest'); // Alias for news latest
Route::get('news/{id}-{slug?}', ['as' => 'news', 'uses' => 'NewsController@show']);
Route::get('news/{id}-{slug?}/out', ['as' => 'news-out', 'uses' => 'NewsController@out']);
Route::get('news/{id}-{slug?}/edit', ['as' => 'news-edit', 'uses' => 'NewsController@edit'])->before('auth');
Route::post('news/{id}-{slug?}/edit', ['as' => 'news-edit', 'uses' => 'NewsController@postEdit'])->before('auth|csrf');
// Questions.
Route::get('questions', ['as' => 'questions', 'uses' => 'QuestionsController@getLatest']); // Alias for questions latest
Route::get('question/{id}-{slug?}', ['as' => 'question', 'uses' => 'QuestionsController@get']);
Route::get('question/{id}-{slug?}/edit', ['as' => 'question-edit', 'uses' => 'QuestionsController@edit'])->before('auth');
Route::post('question/{id}-{slug?}/edit', ['as' => 'question-edit', 'uses' => 'QuestionsController@postEdit'])->before('auth|csrf');

// User
Route::get('user/{username}', ['as' => 'user', 'uses' => 'UsersController@getUser']);
Route::get('user/{username}/post', ['as' => 'user-post', 'uses' => 'UsersController@getPost']);
Route::get('user/{username}/question', ['as' => 'user-question', 'uses' => 'UsersController@getQuestion']);
Route::get('user/{username}/followers', ['as' => 'users-followers', 'uses' => 'UsersController@getFollowers']);
Route::get('user/{username}/following', ['as' => 'users-following', 'uses' => 'UsersController@getFollowing']);
Route::get('user/{username}/collections', ['as' => 'users-collections', 'uses' => 'CollectionController@getUserCollections']);
Route::get('user/{username}/collections/{slug}', ['as' => 'users-collection', 'uses' => 'CollectionController@getCollectionItems']);
Route::get('user/{username}/collections/{slug}/edit', ['as' => 'users-collection-edit', 'uses' => 'CollectionController@getEdit']);
Route::post('user/{username}/collections/{slug}/edit', 'CollectionController@postEdit');

// Blogs
Route::get('blogs', ['as' => 'blogs', 'uses' => 'BlogsController@getIndex']);
Route::get('blog/{id}/{slug}', ['as' => 'blog', 'uses' => 'BlogsController@getBlog']);

// Search
Route::get('search', ['as' => 'serach', 'uses' => 'SearchController@search']);

// Policy
Route::get('tos', ['as' => 'tos', 'uses' => 'PolicyController@getTos']);
Route::get('privacy', ['as' => 'privacy', 'uses' => 'PolicyController@getPrivacy']);
Route::get('faq', ['as' => 'faq', 'uses' => 'PolicyController@getFaq']);
Route::get('about', ['as' => 'about', 'uses' => 'PolicyController@getAbout']);

// Language and queue route
Route::get('lang/{lang?}', function ($lang)
{
    if (in_array($lang, languageArray()))
    {
        Session::put('my.locale', $lang);
    }

    return Redirect::to('/');
});
Route::post('queue/receive', function ()
{
    return Queue::marshal();
});

// Follow Un protection is from controller
Route::group(['before' => 'ajax|ajaxban'], function ()
{
    Route::post('follow', 'FollowController@follow');
    Route::post('deletecomment', 'CommentsController@delete');
    Route::post('votecomment', 'CommentsController@vote');
    Route::post('votereply', 'ReplyController@vote');
    Route::post('deletereply', 'ReplyController@delete');
});

/**
 * Admin section users with admin privileges can access this area
 */
Route::group(['before' => 'admin', 'namespace' => 'Controllers\Admin'], function ()
{
    Route::get('admin', 'IndexController@getIndex');

// Users Manager
    Route::get('admin/users', 'Users\UsersController@getUsersList');
    Route::get('admin/users/featured', 'Users\UsersController@getFeaturedUserList');
    Route::get('admin/users/banned', 'Users\UsersController@getBannedUserList');
    Route::get('admin/user/{username}/edit', 'Users\UsersController@getEditUser');
    Route::get('admin/adduser', 'Users\UsersController@getAddUser');
    Route::post('admin/user/{username}/edit', 'Users\UpdateController@updateUser');
    Route::post('admin/adduser', 'Users\UpdateController@addUser');

// News Manger
    Route::get('admin/news', 'News\NewsController@getAll');
    Route::get('admin/news/featured', 'News\NewsController@getFeatured');
    Route::get('admin/news/approval', 'News\NewsController@getApprovalRequired');
    Route::get('admin/news/{id}/edit', 'News\NewsController@getEdit');
    Route::post('admin/news/{id}/edit', 'News\NewsController@update');
    Route::post('admin/images/approve', 'Images\UpdateController@postApprove');
    Route::post('admin/images/disapprove', 'Images\UpdateController@postDisapprove');

// Questions Manger
    Route::get('admin/questions', 'Questions\QuestionsController@getAll');
    Route::get('admin/questions/featured', 'Questions\QuestionsController@getFeatured');
    Route::get('admin/questions/approval', 'Questions\QuestionsController@getApprovalRequired');
    Route::get('admin/question/{id}/edit', 'Questions\QuestionsController@getEdit');
    Route::post('admin/question/{id}/edit', 'Questions\QuestionsController@update');
    Route::post('admin/question/approve', 'Images\QuestionsController@postApprove');
    Route::post('admin/question/disapprove', 'Images\QuestionsController@postDisapprove');

// Site Settings
    Route::get('admin/sitesettings', 'SiteSettings\SettingsController@getSiteDetails');
    Route::get('admin/sitecategory', 'SiteSettings\SettingsController@getSiteCategory');
    Route::get('admin/limitsettings', 'SiteSettings\SettingsController@getLimitSettings');
    Route::get('admin/removecache', 'SiteSettings\SettingsController@getRemoveCache');
    Route::get('admin/updatesitemap', 'SiteSettings\UpdateController@updateSiteMap');
    Route::post('admin/sitesettings', 'SiteSettings\UpdateController@updateSettings');
    Route::post('admin/limitsettings', 'SiteSettings\UpdateController@postLimitSettings');
    Route::post('admin/sitecategory', 'SiteSettings\UpdateController@createSiteCategory');
    Route::post('admin/sitecategory/reorder', 'SiteSettings\UpdateController@reorderSiteCategory');
    Route::post('admin/sitecategory/update', 'SiteSettings\UpdateController@updateSiteCategory');

// Comments ( done )
    Route::get('admin/comments', 'Comments\CommentsController@getComments');
    Route::get('admin/comment/{id}/edit', 'Comments\CommentsController@getEditComment');
    Route::post('admin/comment/{id}/edit', 'Comments\CommentsController@postEditComment');


// Blogs
    Route::get('admin/blogs', 'Blogs\BlogsController@getBlogs');
    Route::get('admin/blog/create', 'Blogs\BlogsController@getCreate');
    Route::get('admin/blog/{id}/edit', 'Blogs\BlogsController@getEdit');
    Route::post('admin/blog/create', 'Blogs\BlogsController@postCreate');
    Route::post('admin/blog/{id}/edit', 'Blogs\BlogsController@postEdit');

//Bulk upload
    Route::get('admin/bulkupload', 'Bulkupload\BulkuploadController@getBulkUpload');
    Route::post('admin/bulkupload', 'Bulkupload\BulkuploadController@postBulkUpload');

// Reports
    Route::get('admin/reports', 'Reports\ReportsController@getReports');
    Route::get('admin/report/{id}', 'Reports\ReportsController@getReadReport');

});