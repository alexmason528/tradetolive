<?php

namespace Controllers\Admin\Reports;

use Carbon\Carbon;
use DB;
use Hackersnews\Repository\FlagsRepositoryInterface;
use Report;
use View;

class ReportsController extends \BaseController {

    /**
     * @var \Hackersnews\Repository\FlagsRepositoryInterface
     */
    private $flags;

    public function  __construct(FlagsRepositoryInterface $flags)
    {
        $this->flags = $flags;
    }

    public function getReports()
    {
        $reports = $this->flags->getAll();

        return View::make('admin/reports/index')
            ->with('reports', $reports)
            ->with('title', 'Latest Reports');
    }

    public function getReadReport($id)
    {
        $report = $this->flags->getById($id);
        $report->checked_at = Carbon::now();
        $report->save();

        return View::make('admin/reports/read')
            ->with('title', 'Full Report')
            ->with('report', $report);
    }
}