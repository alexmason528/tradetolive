<?php
/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
use Hackersnews\Repository\PostsRepositoryInterface;

class BrowseController extends BaseController {

    /**
     * @param PostsRepositoryInterface $posts
     */
    public function __construct(PostsRepositoryInterface $posts)
    {
        $this->posts = $posts;
    }

    /**
     * @return mixed
     */
    public function getTrending()
    {
        $posts = $this->posts->getTrending(null, ['category' => Input::get('category'), 'timeframe' => Input::get('timeframe')]);

        return View::make('post.list')->with('title', t('Trending'))->with('posts', $posts);
    }

    /**
     * @return mixed
     */
    public function getLatest()
    {
        $posts = $this->posts->getLatest(null, ['category' => Input::get('category'), 'timeframe' => Input::get('timeframe')]);
        $title = t('Latest');

        return View::make('post.list', compact('title', 'posts'));
    }

    /**
     * @return mixed
     */
    public function getFeatured()
    {
        $posts = $this->posts->getFeatured(null, ['category' => Input::get('category'), 'timeframe' => Input::get('timeframe')]);
        $title = t('Featured');

        return View::make('post.list', compact('title', 'posts'));
    }

    /**
     * @return mixed
     */
    public function getArchive()
    {
        $results = $this->posts->getArchive(null, ['category' => Input::get('category'), 'timeframe' => Input::get('timeframe')]);

        $total = count($results);
        $start = (Paginator::getCurrentPage() - 1) * perPage();
        $posts = array_slice($results, $start, perPage());
        $pagination = Paginator::make($results, $total, perPage());

        $title = t('Archive');

        return View::make('post.archive', compact('title', 'posts', 'pagination'));
    }
}