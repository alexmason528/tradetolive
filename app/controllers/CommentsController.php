<?php

/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
use Hackersnews\Repository\CommentsRepositoryInterface;
use Hackersnews\Validator\CommentsValidator;

class CommentsController extends BaseController {

    /**
     * @var Hackersnews\Repository\CommentsRepositoryInterface
     */
    private $comments;
    /**
     * @var Hackersnews\Validator\CommentsValidator
     */
    private $validator;


    /**
     * @param CommentsValidator                                  $validator
     * @param Hackersnews\Repository\CommentsRepositoryInterface $comment
     */
    public function __construct(CommentsValidator $validator, CommentsRepositoryInterface $comment)
    {
        $this->comment = $comment;
        $this->validator = $validator;
    }

    /**
     * @param      $id
     * @param null $slug
     * @return mixed
     */
    public function create($id, $slug = null)
    {
        if ( ! $id || ! $slug)
        {
            return Redirect::back()->with('flashError', t('Please try again'));
        }

        if ( ! $this->validator->validCreate(Input::all()))
        {
            return Redirect::back()->withErrors($this->validator->errors());
        }

        $this->comment->create($id, Input::all());

        return Redirect::back()->with('flashSuccess', t('Comment is posted'));
    }

    public function vote()
    {
        $id = Input::get('id');
        if (Auth::check() == false)
        {
            return Response::make('error', 404);
        }
        if ( ! $id)
        {
            return Response::make('error', 404);
        }

        $comment = $this->comment->getById($id);

        if ( ! $comment)
        {
            return Response::make('error', 404);
        }

        $voted = $comment->votes()->whereUserId(Auth::user()->id)->first();

        if ( ! $voted)
        {
            $this->comment->vote($id);
        }
        else
        {
            $this->comment->deleteVote($id);
        }

        return Response::make($comment->votes()->count(), 200);
    }

    public function delete()
    {
        if ( ! Auth::check())
        {
            return '';
        }

        if ($this->comment->delete(Input::get('id')))
        {
            return 'success';
        }

        return 'failed';
    }
}