<?php

/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
use Hackersnews\Repository\PostsRepositoryInterface;

class RssController extends BaseController {

    public function __construct(PostsRepositoryInterface $posts)
    {
        $this->posts = $posts;
    }

    public function getRss()
    {
        if (Input::get('type') === 'questions' || Input::get('type') === 'news')
        {
            switch (Input::get('only'))
            {
                case 'latest':
                    $posts = $this->posts->getLatest(null, ['category' => Input::get('category'), 'timeframe' => Input::get('timeframe')]);
                    break;
                case 'trending':
                    $posts = $this->posts->getTrending(null, ['category' => Input::get('category'), 'timeframe' => Input::get('timeframe')]);
                    break;
                case 'featured':
                    $posts = $this->posts->getFeatured(null, ['category' => Input::get('category'), 'timeframe' => Input::get('timeframe')]);
                    break;
                default:
                    $posts = $this->posts->getLatest(null, ['category' => Input::get('category'), 'timeframe' => Input::get('timeframe')]);
            }

            return $this->generateRss($posts);
        }

        return $this->generateRss($this->posts->getTrending(null, ['category' => Input::get('category'), 'timeframe' => Input::get('timeframe')]));

    }


    public function generateRss($posts)
    {
        $feed = Feed::make();
        $feed->title = siteSettings('siteName');
        $feed->description = siteSettings('siteName');
        $feed->lang = 'en';
        $feed->link = URL::to('rss');

        foreach ($posts as $post)
        {
            $desc = '<h2><a href="' . route($post->type, ['id' => $post->id, 'slug' => $post->slug]) . '">' . e($post->title) . '</a>
                by
                <a href="' . url('user/' . $post->user->username) . '">' . ucfirst($post->user->fullname) . '</a>
                ( <a href="' . url('user/' . $post->user->username) . '">' . $post->user->username . '</a> )
                </h2>' . $post->summary;
            $feed->add(ucfirst($post->title), $post->user->fullname, route($post->type, ['id' => $post->id, 'slug' => $post->slug]), $post->created_at, $desc);
        }

        return $feed->render('atom');
    }

}