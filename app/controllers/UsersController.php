<?php

/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */

use Hackersnews\Repository\CollectionsRepositoryInterface;
use Hackersnews\Repository\UsersRepositoryInterface;
use Hackersnews\Validator\UsersValidator;

class UsersController extends BaseController {

    /**
     * @param UsersRepositoryInterface $user
     * @param UsersValidator           $validator
     */
    public function __construct(CollectionsRepositoryInterface $collection, UsersRepositoryInterface $user, UsersValidator $validator)
    {
        $this->user = $user;
        $this->validator = $validator;
        $this->collection = $collection;
    }

    /**
     * @param $username
     * @return mixed
     */
    public function getUser($username)
    {
        /*$user = $this->user->getByUsername($username);

        $posts = $user->posts()->with('comments', 'votes', 'user', 'category')->paginate(perPage());

        return View::make('user/user', compact('user', 'posts'));*/

        return Redirect::route('user-post', ['username' => Auth::user()->username]);
    }

    /**
     * @param $username
     * @return mixed
     */
    public function getPost($username)
    {
        $user = $this->user->getByUsername($username);

        $posts = $user->posts()->with('comments', 'votes', 'user', 'category')->where('type', 'news')->paginate(perPage());

        $news_cnt = $user->posts()->with('comments', 'votes', 'user', 'category')->where('type', 'news')->count();
        $question_cnt = $user->posts()->with('comments', 'votes', 'user', 'category')->where('type', 'question')->count();
        $notification_cnt = $this->user->notifications(Auth::user()->id)->count();
        $collection_cnt = $this->collection->getUsersCollections($user)->count();

        $count = array(
            'news' => $news_cnt,
            'question' => $question_cnt,
            'notification' => $notification_cnt,
            'collection' => $collection_cnt
        );

        return View::make('user/user', compact('user', 'posts', 'count'));
    }

    /**
     * @param $username
     * @return mixed
     */
    public function getQuestion($username)
    {
        $user = $this->user->getByUsername($username);

        $posts = $user->posts()->with('comments', 'votes', 'user', 'category')->where('type', 'question')->paginate(perPage());

        $news_cnt = $user->posts()->with('comments', 'votes', 'user', 'category')->where('type', 'news')->count();
        $question_cnt = $user->posts()->with('comments', 'votes', 'user', 'category')->where('type', 'question')->count();
        $notification_cnt = $this->user->notifications(Auth::user()->id)->count();
        $collection_cnt = $this->collection->getUsersCollections($user)->count();

        $count = array(
            'news' => $news_cnt,
            'question' => $question_cnt,
            'notification' => $notification_cnt,
            'collection' => $collection_cnt
        );


        return View::make('user/user', compact('user', 'posts', 'count'));
    }

    /**
     * @return mixed
     */
    public function getSettings()
    {
        $user = $this->user->getByUsername(Auth::user()->username);
        return View::make('user/settings', compact('user'));
    }

    /**
     * @return mixed
     */
    public function updateProfile()
    {
        if ( ! $this->validator->validUpdate(Input::all()))
        {
            return Redirect::route('settings')->withErrors($this->validator->errors())->withInput(Input::all());
        }

        Auth::user()->fullname = Input::get('fullname');
        Auth::user()->gender = Input::get('gender');
        Auth::user()->fb_link = Input::get('fbLink');
        Auth::user()->tw_link = Input::get('twLink');
        Auth::user()->blogurl = Input::get('blogurl');
        Auth::user()->save();

        return Redirect::route('settings')->with('flashSuccess', t('Your profile is now updated'));
    }

    /**
     * @return mixed
     */
    public function mailsettings()
    {
        if ( ! $this->validator->validEmailSettings(Input::all()))
        {
            return Redirect::to('settings')->withErrors($this->validator->errors());
        }
        Input::get('email_comment') ? Auth::user()->email_comment = 1 : Auth::user()->email_comment = 0;
        Input::get('email_reply') ? Auth::user()->email_reply = 1 : Auth::user()->email_reply = 0;
        Input::get('email_follow') ? Auth::user()->email_follow = 1 : Auth::user()->email_follow = 0;
        Input::get('email_vote') ? Auth::user()->email_vote = 1 : Auth::user()->email_vote = 0;
        Auth::user()->save();

        return Redirect::back()->with('flashSuccess', t('Your email settings are now updated'));
    }

    /**
     * @return mixed
     */
    public function changePassword()
    {
        if ( ! $this->validator->validPasswordUpdate(Input::all()))
        {
            return Redirect::to('settings')->withErrors($this->validator->errors());
        }

        if ( ! $this->user->updatePassword(Input::all()))
        {
            return Redirect::to('settings')->with('flashError', t('Old password is not valid'));
        }

        return Redirect::to('settings')->with('flashSuccess', t('Your password is updated'));
    }

    /**
     * @return mixed
     */
    public function getNotifications()
    {
        $notifications = $this->user->notifications(Auth::user()->id);
        $user = $this->user->getByUsername(Auth::user()->username);

        $news_cnt = $user->posts()->with('comments', 'votes', 'user', 'category')->where('type', 'news')->count();
        $question_cnt = $user->posts()->with('comments', 'votes', 'user', 'category')->where('type', 'question')->count();
        $notification_cnt = $this->user->notifications(Auth::user()->id)->count();
        $collection_cnt = $this->collection->getUsersCollections($user)->count();

        $count = array(
            'news' => $news_cnt,
            'question' => $question_cnt,
            'notification' => $notification_cnt,
            'collection' => $collection_cnt
        );

        return View::make('user/notifications', compact('notifications', 'user', 'count'));
    }

    /**
     * @return mixed
     */
    public function getFeeds()
    {
        $posts = $this->user->getFeeds(Input::get('category'));
        $title = t('Feeds from following users');

        return View::make('post/list', compact('posts', 'title'));
    }

    /**
     * @param $username
     * @return mixed
     */
    public function getFollowers($username)
    {
        $user = $this->user->getByUsername($username);
        $title = t('Followers');

        $followers = $this->user->getFollowers($user);


        return View::make('user/followers', compact('user', 'title', 'followers'));
    }

    /**
     * @param $username
     * @return mixed
     */
    public function getFollowing($username)
    {
        $user = $this->user->getByUsername($username);
        $title = t('Followers');
        $followers = $this->user->getFollowing($user);

        return View::make('user/followers', compact('user', 'title', 'followers'));
    }

    /**
     * @return mixed
     */
    public function updateAvatar()
    {
        if ( ! $this->validator->validAvatarUpdate(Input::all()))
        {
            return Redirect::back()->withErrors($this->validator->errors());
        }

        if ( ! Input::hasFile('avatar'))
        {
            return Redirect::back()->with('flashError', t('Please try again'));
        }

        $name = $this->dirName('avatars') . '.' . Input::file('avatar')->getClientOriginalExtension();
        $success = Input::file('avatar')->move('avatars', $name);
        if ($success)
        {
            Auth::user()->avatar = $name;
            Auth::user()->save();

            return Redirect::back()->with('flashSuccess', t('Avatar is now updated'));
        }

        return Redirect::back()->with('flashError', t('Please try again'));
    }

    /**
     * @return string
     */
    protected function dirName($name)
    {
        $str = str_random(9);
        if (file_exists(public_path() . $name . '/' . $str))
        {
            $str = $this->dirName($name);
        }

        return $str;
    }
}