<?php
/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
use Hackersnews\Repository\BlogsRepositoryInterface;

class BlogsController extends BaseController {

    public function __construct(BlogsRepositoryInterface $blogs)
    {
        $this->blogs = $blogs;
    }

    public function getIndex()
    {
        $blogs = $this->blogs->getLatestBlogs(5);
        $title = t('All Blogs');

        return View::make('blogs/list', compact('blogs', 'title'));
    }

    public function getBlog($id, $slug)
    {
        $post = $this->blogs->get($id);

        if ( ! $post)
        {
            return Redirect::to('/');
        }

        if (empty($slug) || $slug != $post->slug)
        {
            return Redirect::to('blog/' . $post->id . '/' . $post->slug);
        }

        $title = $post->title;

        return View::make('blogs/blog', compact('post', 'title'));
    }
}