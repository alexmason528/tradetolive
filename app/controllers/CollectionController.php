<?php
use Hackersnews\Repository\CollectionsRepositoryInterface;
use Hackersnews\Repository\Eloquent\PostsRepository;
use Hackersnews\Repository\UsersRepositoryInterface;
use Hackersnews\Validator\CollectionValidator;

/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
class CollectionController extends BaseController {

    /**
     * @param CollectionsRepositoryInterface $collection
     * @param UsersRepositoryInterface       $user
     * @param PostsRepository                $post
     */
    public function __construct(CollectionsRepositoryInterface $collection, CollectionValidator $validator, UsersRepositoryInterface $user, PostsRepository $post)
    {
        $this->collection = $collection;
        $this->user = $user;
        $this->post = $post;
        $this->validator = $validator;
    }

    /**
     * @return mixed
     */
    public function getCreate()
    {
        return View::make('collection/create');
    }

    /**
     * @return mixed
     */
    public function postCreate()
    {
        if ( ! $this->validator->validCreate(Input::all()))
        {
            return Redirect::back()->withErrors($this->validator->errors());
        }
        $this->collection->create(Input::all());

        return Redirect::route('collections')->with('flashSuccess', t('Collection is now created successfully'));
    }

    /**
     * @param $username
     * @param $slug
     * @return mixed
     */
    public function getEdit($username, $slug)
    {
        $post = $this->collection->getBySlug($slug);
        if ($post->user_id != Auth::user()->id)
        {
            return Redreict::route('home')->with('flashError', t('You are not allowed'));
        }

        return View::make('collection/edit', compact('post'));
    }

    /**
     * @param $username
     * @param $slug
     * @return mixed
     */
    public function postEdit($username, $slug)
    {
        if ( ! $this->validator->validCreate(Input::all()))
        {
            return Redirect::back()->withErrors($this->validator->errors());
        }

        $post = $this->collection->getBySlug($slug);

        if ($post->user_id !== Auth::user()->id)
        {
            return Redreict::route('home')->with('flashError', t('You are not allowed'));
        }
        $this->collection->update($post, Input::all());

        return Redirect::route('users-collection-edit', ['username' => $username, 'slug' => $slug])->with('flashSuccess', t('Your collection is now updated'));
    }

    /**
     * @param $username
     * @return mixed
     */
    public function getUserCollections($username)
    {
        $user = $this->user->getByUsername($username);

        $collections = $this->collection->getUsersCollections($user);

        $news_cnt = $user->posts()->with('comments', 'votes', 'user', 'category')->where('type', 'news')->count();
        $question_cnt = $user->posts()->with('comments', 'votes', 'user', 'category')->where('type', 'question')->count();
        $notification_cnt = $this->user->notifications(Auth::user()->id)->count();
        $collection_cnt = $this->collection->getUsersCollections($user)->count();

        $count = array(
            'news' => $news_cnt,
            'question' => $question_cnt,
            'notification' => $notification_cnt,
            'collection' => $collection_cnt
        );

        return View::make('user/collections', compact('user', 'collections', 'count'));
    }

    /**
     * @param $username
     * @param $slug
     * @return mixed
     */
    public function getCollectionItems($username, $slug)
    {
        $user = $this->user->getByUsername($username);
        $collection = $this->collection->getBySlug($slug);
        Event::fire('collection.views', $collection);
        $posts = $this->collection->getCollectionItems($collection);

        return View::make('user/collectionItems', compact('user', 'collection', 'posts'));
    }


    /**
     * @return mixed
     */
    public function addPost()
    {
        $collection = $this->collection->getById(Input::get('id'));
        $this->post->getById(Input::get('post_id'));

        if ($collection->user_id !== Auth::user()->id || ! $this->collection->addToCollection(Input::all()))
        {
            return Redirect::back()->with('flashError', t('Please try again'));
        }

        return Redirect::back()->with('flashSuccess', t('Post is now added to your collection'));
    }

    /**
     * @return string
     */
    public function removeItem()
    {
        if ($this->collection->removeItem(Input::all()))
        {
            return 'success';
        }

    }

    /**
     * @return mixed
     */
    public function getPopularCollection()
    {
        $collections = $this->collection->getPopularCollection();
        $title = t('Top Collections');

        return View::make('collection/list', compact('collections', 'title'));
    }

}