<?php

/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
use Hackersnews\Repository\PostsRepositoryInterface;

class SearchController extends BaseController {

    /**
     * @param PostsRepositoryInterface $posts
     */
    public function __construct(PostsRepositoryInterface $posts)
    {
        $this->posts = $posts;
    }

    /**
     * @return mixed
     */
    public function search()
    {
        $posts = $this->posts->search(Input::get('q'));
        $title = t('Search Results');

        return View::make('post/list', compact('posts', 'title'));
    }
}