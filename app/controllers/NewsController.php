<?php

use Hackersnews\Repository\PostsRepositoryInterface;
use Hackersnews\Repository\VotesRepositoryInterface;
use Hackersnews\Validator\NewsValidator;

class NewsController extends BaseController {

    /**
     * @param PostsRepositoryInterface $news
     * @param NewsValidator            $validator
     * @param VotesRepositoryInterface $votes
     */
    public function __construct(PostsRepositoryInterface $news, NewsValidator $validator, VotesRepositoryInterface $votes)
    {
        $this->news = $news;
        $this->validator = $validator;
        $this->votes = $votes;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return View::make('post/submit');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if ( ! $this->validator->validCreate(Input::all()))
        {
            return Redirect::back()->withInput()->withErrors($this->validator->errors());
        }
        $news = $this->news->create('news', Input::all());

        if ((bool) siteSettings('autoApprove') == 0)
        {
            return Redirect::route('home')->with('flashSuccess', t('Your post is submitted and waiting for approval, please keep patience'));
        }

        return Redirect::route('news', ['id' => $news->id, 'slug' => $news->slug]);
    }


    /**
     * @param      $id
     * @param null $slug
     * @return mixed
     */
    public function show($id, $slug = null)
    {
        $post = $this->news->getById($id, 'news');
        if ($slug != $post->slug)
        {
            return Redirect::route('news', ['id' => $post->id, 'slug' => $post->slug]);
        }
        Event::fire('posts.views', $post);

        $next = $this->news->findNextPost($post);
        $previous = $this->news->findPreviousPost($post);
        $related = $this->news->findRelatedPosts($post);


        $comments = $post->comments()->with('votes', 'votes.user', 'user', 'reply', 'reply.user', 'reply.comment', 'reply.votes', 'reply.votes.user')->orderBy('created_at', 'desc')->paginate(perPage());
        $title = t('News');

        return View::make('post/post', compact('post', 'comments', 'title', 'next', 'previous', 'related'));
    }


    /**
     * @return mixed
     */
    public function getTrending()
    {
        $posts = $this->news->getTrending('news', ['category' => Input::get('category'), 'timeframe' => Input::get('timeframe')]);
        $title = t('Trending News');

        return View::make('post.list', compact('posts', 'title'));
    }

    /**
     * @return mixed
     */
    public function getLatest()
    {
        $posts = $this->news->getLatest('news', ['category' => Input::get('category'), 'timeframe' => Input::get('timeframe')]);
        $title = t('Latest News');

        return View::make('post.list', compact('posts', 'title'));
    }

    /**
     * @return mixed
     */
    public function getFeatured()
    {
        $posts = $this->news->getFeatured('news', ['category' => Input::get('category'), 'timeframe' => Input::get('timeframe')]);
        $title = t('Featured News');

        return View::make('post.list', compact('posts', 'title'));
    }

    /**
     * @return mixed
     */
    public function vote()
    {
        $id = Input::get('id');

        $news = $this->news->getAnyById($id);

        $voted = $news->votes()->whereUserId(Auth::user()->id)->first();
        if ( ! $voted)
        {
            $this->votes->vote($news);
        }
        else
        {
            $voted->delete();
        }

        return Response::make($news->votes()->count(), 200);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $post = $this->news->getById($id);
        $title = 'Editing';
        if ($post->user->id !== Auth::user()->id)
        {
            return Redirect::route('home')->with('flashError', t('Nothing Found'));
        }

        return View::make('post/edit', compact('post', 'title'));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function postEdit($id)
    {
        if ( ! $this->validator->validEdit(Input::all()))
        {
            return Redirect::back()->withInput()->withErrors($this->validator->errors());
        }
        $post = $this->news->getById($id);
        $news = $this->news->edit($post, Input::all());
        if (Input::get('delete'))
        {
            return Redirect::route('home')->with('flashSuccess', t('Post is now deleted'));
        }

        return Redirect::route('news', ['id' => $news->id, 'slug' => $news->slug])->with('flashSuccess', t('Post is now update'));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function out($id)
    {
        $post = $this->news->getById($id);

        Event::fire('posts.views', $post);

        return Redirect::to(filter_var($post->url, FILTER_SANITIZE_URL));
    }
}
