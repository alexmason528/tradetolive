<?php

/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
use Hackersnews\Repository\PostsRepositoryInterface;
use Hackersnews\Validator\QuestionsValidator;

class QuestionsController extends BaseController {

    /**
     * @param PostsRepositoryInterface $question
     * @param QuestionsValidator       $validator
     */
    public function __construct(PostsRepositoryInterface $question, QuestionsValidator $validator)
    {
        $this->question = $question;
        $this->validator = $validator;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return View::make('questions/submit');
    }

    /**
     * @return mixed
     */
    public function create()
    {
        if ( ! $this->validator->validCreate(Input::all()))
        {
            return Redirect::back()->withInput()->withErrors($this->validator->errors());
        }
        $question = $this->question->create('question', Input::all());
        if ((bool) siteSettings('autoApprove') == 0)
        {
            return Redirect::route('home')->with('flashSuccess', t('Your post is submitted and waiting for approval, please keep patience'));
        }

        return Redirect::route('question', ['id' => $question->id, 'slug' => $question->slug]);
    }

    /**
     * @param      $id
     * @param null $slug
     * @return mixed
     */
    public function get($id, $slug = null)
    {
        $post = $this->question->getById($id, 'question');
        if ( ! $post || ! $post->approved_at)
        {
            return Redirect::route('trending')->with('flashError', t('Nothing Found'));
        }
        if ($slug != $post->slug)
        {
            return Redirect::route('question', ['id' => $post->id, 'slug' => $post->slug]);
        }
        Event::fire('posts.views', $post);

        $next = $this->question->findNextPost($post);
        $previous = $this->question->findPreviousPost($post);
        $related = $this->question->findRelatedPosts($post);

        $comments = $post->comments()->with('votes', 'votes.user', 'user', 'reply', 'reply.user', 'reply.comment', 'reply.votes', 'reply.votes.user')->orderBy('created_at', 'desc')->paginate(perPage());
        $title = t('Question');

        return View::make('post/post', compact('post', 'comments', 'title', 'next', 'previous', 'related'));
    }

    /**
     * @return mixed
     */
    public function getLatest()
    {
        $posts = $this->question->getLatest('question', ['category' => Input::get('category'), 'timeframe' => Input::get('timeframe')]);
        $title = t('Latest Questions');

        return View::make('post/list', compact('posts', 'title'));
    }


    /**
     * @return mixed
     */
    public function getTrending()
    {
        $posts = $this->question->getTrending('question', ['category' => Input::get('category'), 'timeframe' => Input::get('timeframe')]);
        $title = t('Trending Questions');

        return View::make('post/list', compact('posts', 'title'));
    }

    /**
     * @return mixed
     */
    public function getFeatured()
    {
        $posts = $this->question->getFeatured('question', ['category' => Input::get('category'), 'timeframe' => Input::get('timeframe')]);
        $title = t('Featured Questions');

        return View::make('post/list', compact('posts', 'title'));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $post = $this->question->getById($id);
        $title = 'Editing ' . $post->title;
        if ( ! $post || $post->user->id !== Auth::user()->id)
        {
            return Redirect::route('home')->with('flashError', t('Nothing Found'));
        }

        return View::make('post/edit', compact('post', 'title'));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function postEdit($id)
    {
        $post = $this->question->getById($id);

        if ( ! $this->validator->validEdit(Input::all()))
        {
            return Redirect::back()->withInput()->withErrors($this->validator->errors());
        }

        $post = $this->question->edit($post, Input::all());

        if (Input::get('delete'))
        {
            return Redirect::route('home')->with('flashSuccess', t('Post is now deleted'));
        }

        return Redirect::route('question', ['id' => $post->id, 'slug' => $post->slug])->with('flashSuccess', t('Post is now update'));
    }
}