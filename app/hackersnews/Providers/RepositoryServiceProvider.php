<?php
/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
namespace Hackersnews\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'Hackersnews\Repository\PostsRepositoryInterface',
            'Hackersnews\Repository\Eloquent\PostsRepository'
        );

        $this->app->bind(
            'Hackersnews\Repository\UsersRepositoryInterface',
            'Hackersnews\Repository\Eloquent\UsersRepository'
        );

        $this->app->bind(
            'Hackersnews\Repository\BlogsRepositoryInterface',
            'Hackersnews\Repository\Eloquent\BlogsRepository'
        );

        $this->app->bind(
            'Hackersnews\Repository\FlagsRepositoryInterface',
            'Hackersnews\Repository\Eloquent\FlagsRepository'
        );

        $this->app->bind(
            'Hackersnews\Repository\CommentsRepositoryInterface',
            'Hackersnews\Repository\Eloquent\CommentsRepository'
        );

        $this->app->bind(
            'Hackersnews\Repository\ReplyRepositoryInterface',
            'Hackersnews\Repository\Eloquent\ReplyRepository'
        );

        $this->app->bind(
            'Hackersnews\Repository\VotesRepositoryInterface',
            'Hackersnews\Repository\Eloquent\VotesRepository'
        );

        $this->app->bind(
            'Hackersnews\Repository\FollowRepositoryInterface',
            'Hackersnews\Repository\Eloquent\FollowRepository'
        );

        $this->app->bind(
            'Hackersnews\Repository\CollectionsRepositoryInterface',
            'Hackersnews\Repository\Eloquent\CollectionsRepository'
        );
    }
}