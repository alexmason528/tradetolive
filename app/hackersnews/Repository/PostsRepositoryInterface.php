<?php

/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
namespace Hackersnews\Repository;

use Post;

interface PostsRepositoryInterface {

    /**
     * @param      $id
     * @param null $type
     * @return mixed
     */
    public function getById($id, $type = null);

    /**
     * @param $id
     * @return mixed
     */
    public function getAnyById($id);

    /**
     * @param null  $type
     * @param array $param
     * @return mixed
     */
    public function getTrending($type = null, $param = []);

    /**
     * @param null  $type
     * @param array $param
     * @return mixed
     */
    public function getLatest($type = null, $param = []);

    /**
     * @param null  $type
     * @param array $param
     * @return mixed
     */
    public function getFeatured($type = null, $param = []);

    /**
     * @param null  $type
     * @param array $param
     * @return mixed
     */
    public function getArchive($type = null, $param = []);

    /**
     * @param $news
     * @return mixed
     */
    public function incrementViews($news);

    /**
     * @param       $term
     * @param null  $type
     * @param array $param
     * @return mixed
     */
    public function search($term, $type = null, $param = []);

    /**
     * @param       $type
     * @param array $input
     * @return mixed
     */
    public function create($type, array $input);

    /**
     * @param $list
     * @param $category
     * @return mixed
     */
    public function getFeedsForUser($list, $category);

    /**
     * @param Post  $posts
     * @param array $input
     * @return Post
     */
    public function edit(Post $posts, array $input);

    //@todo add filter for admin panel
    /**
     * @return mixed
     */
    public function getApprovalRequired();

    /**
     * Admin helper, gives output irr-respective of post approved or not
     *
     * @param $id
     * @return mixed
     */
    public function getByIdForAdmin($id);

    /**
     * Admin helper, to edit post irr-respective of post approved or not
     *
     * @param Post  $posts
     * @param array $input
     * @return Post
     */
    public function adminEdit(Post $posts, array $input);
}