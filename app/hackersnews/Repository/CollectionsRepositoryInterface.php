<?php
/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
namespace Hackersnews\Repository;

use Collection;
use User;

interface CollectionsRepositoryInterface {

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param $slug
     * @return mixed
     */
    public function getBySlug($slug);

    /**
     * @param User $id
     * @return mixed
     */
    public function getUsersCollections(User $id);

    /**
     * @param Collection $collection
     * @return mixed
     */
    public function getCollectionItems(Collection $collection);

    /**
     * @param $input
     * @return mixed
     */
    public function create($input);

    /**
     * @param $input
     * @return mixed
     */
    public function addToCollection($input);

    /**
     * @param $collection
     * @return mixed
     */
    public function incrementViews($collection);
}