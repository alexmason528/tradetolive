<?php
/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
namespace Hackersnews\Repository\Eloquent;

use Collection;
use CollectionItems;
use Hackersnews\Repository\CollectionsRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Post;
use User;

class CollectionsRepository extends AbstractRepository implements CollectionsRepositoryInterface {

    public function __construct(Collection $model, CollectionItems $items, Post $posts)
    {

        $this->model = $model;
        $this->items = $items;
        $this->posts = $posts;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return $this->model->whereId($id)->firstOrFail();
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function getBySlug($slug)
    {
        return $this->model->whereSlug($slug)->firstOrFail();
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function getUsersCollections(User $user)
    {
        $collections = $user->collections()->with('items', 'items.posts')->paginate(perPage());

        return $collections;
    }

    /**
     * @param Collection $collection
     * @return mixed
     */
    public function getCollectionItems(Collection $collection)
    {
        $posts = $this->items->whereCollectionId($collection->id)
            ->with('posts', 'posts.user', 'posts.votes', 'posts.comments')->paginate(perPage());

        return $posts;
    }

    /**
     * @param $input
     */
    public function create($input)
    {
        $collection = $this->getNew();
        $collection->user_id = Auth::user()->id;
        $collection->title = $input['title'];
        $slug = $this->generateSlug($input['title']);
        $collection->slug = $slug;
        $collection->summary = $input['summary'];
        $collection->views = 0;
        $collection->save();
    }

    /**
     * @param Collection $collection
     * @param            $input
     * @return Collection
     */
    public function update(Collection $collection, $input)
    {
        if (isset($input['delete']))
        {
            $collection->items()->delete();
            $collection->delete();

            return $collection;
        }
        $collection->title = $input['title'];
        $collection->summary = $input['summary'];
        $collection->save();

        return $collection;
    }

    /**
     * @param $input
     * @return bool|CollectionItems
     */
    public function addToCollection($input)
    {
        $collection = $this->getById($input['id']);
        $item = $this->items->whereCollectionId($input['id'])->whereUserId(Auth::user()->id)->wherePostId($input['post_id'])->first();

        if ($collection->user_id !== Auth::user()->id || $item)
        {
            return false;
        }
        $newItem = new CollectionItems();
        $newItem->user_id = Auth::user()->id;
        $newItem->collection_id = $input['id'];
        $newItem->post_id = $input['post_id'];
        $newItem->save();

        return $newItem;
    }

    /**
     * @return mixed
     */
    public function getPopularCollection()
    {
        $posts = $this->model->with('items', 'user')
            ->leftJoin('collection_items', 'collections.id', '=', 'collection_items.collection_id')
            ->select('collections.*', DB::raw('count(collection_items.collection_id) * 3 + collections.views * 5 as popular'))
            ->groupBy('collections.id')->with('user')->orderBy('popular', 'desc')
            ->paginate(perPage());

        return $posts;
    }

    /**
     * @param $input
     * @return bool
     */
    public function removeItem($input)
    {
        $collection = $this->model->whereId($input['id'])->first();
        $item = $this->items->whereCollectionId($collection->id)->wherePostId($input['post_id'])->first();
        if (($collection && $item) && ($collection->user_id == Auth::user()->id && $item->user_id == Auth::user()->id))
        {
            $item->delete();

            return true;
        }

        return false;
    }

    /**
     * @param $collection
     * @return mixed
     */
    public function incrementViews($collection)
    {
        $collection->views = $collection->views + 1;
        $collection->save();

        return $collection;
    }

    /**
     * @param $title
     * @return string
     */
    protected function generateSlug($title)
    {
        $slug = @Str::slug($title);
        if ( ! $slug)
        {
            $slug = Str::random(4);
        }
        $slug = $this->getExistingSlugs($slug);

        return $slug;
    }

    /**
     * @param $slug
     * @return string
     */
    protected function getExistingSlugs($slug)
    {
        $query = $this->model->where('slug', 'LIKE', $slug . '%');
        $list = $query->lists('slug', 'slug');

        if (
            count($list) === 0 ||
            ! in_array($slug, $list) ||
            (array_key_exists('slug', $list) && $list['slug'] === $slug)
        )
        {
            return $slug;
        }

        $len = strlen($slug . '-');
        array_walk($list, function (&$value, $key) use ($len)
        {
            $value = intval(substr($value, $len));
        });

        rsort($list);
        $increment = reset($list) + 1;

        return $slug . '-' . $increment;
    }
}