<?php

/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
namespace Hackersnews\Repository\Eloquent;

use Auth;
use Flags;
use Hackersnews\Repository\FlagsRepositoryInterface;
use Post;
use User;

class FlagsRepository extends AbstractRepository implements FlagsRepositoryInterface {

    /**
     * @var \Flags
     */
    protected $model;

    public function  __construct(Flags $flags)
    {
        $this->model = $flags;
    }

    public function getById($id)
    {
        return $this->model->whereId($id)->first();
    }

    public function getAll()
    {
        return $this->model->orderBy('created_at', 'desc')->get();
    }

    public function report(Post $post, $reason)
    {
        $report = $this->getNew();
        $report->post_id = $post->id;
        $report->type = $post->type;
        $report->user_id = Auth::user()->id;
        $report->reason = $reason;
        $report->reported_user = null;
        $report->save();

        return true;
    }

    public function reportUser(User $user, $reason)
    {
        $report = $this->getNew();
        $report->post_id = null;
        $report->reported_user = $user->id;
        $report->type = 'user';
        $report->user_id = Auth::user()->id;
        $report->reason = $reason;
        $report->save();

        return true;
    }
}