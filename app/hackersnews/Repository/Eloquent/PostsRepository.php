<?php
/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
namespace Hackersnews\Repository\Eloquent;

use Auth;
use Carbon\Carbon;
use Category;
use DB;
use Favorite;
use Hackersnews\Repository\PostsRepositoryInterface;
use Post;
use Purifier;
use Str;

class PostsRepository extends AbstractRepository implements PostsRepositoryInterface {

    /**
     * @var \Post
     */
    protected $model;

    /**
     * @param Post     $posts
     * @param Category $category
     */
    public function __construct(Post $posts, Category $category)
    {
        $this->model = $posts;
        $this->category = $category;
    }

    /**
     * @param      $id
     * @param null $type
     * @return mixed
     */
    public function getById($id, $type = null)
    {
        $post = $this->posts($type)->whereId($id)->firstOrFail();

        return $post;
    }

    /**
     * @param null  $type
     * @param array $param
     * @return Post
     */
    private function posts($type = null, $param = [])
    {
        $post = $this->model->approved();
        if (isset($param['category']))
        {
            if ($category = $this->category->whereSlug($param['category'])->first())
            {
                $post = $post->whereCategoryId($category->id);
            }
        }
        if ($type)
        {
            $post = $post->whereType($type);
        }
        if (isset($param['timeframe']))
        {
            if ($timeframe = $this->resolveTime($param['timeframe']))
            {
                $post = $post->whereBetween('approved_at', $timeframe);
            }
        }

        return $post;
    }

    /**
     * @param $time
     * @return string
     */
    private function resolveTime($time)
    {
        switch ($time)
        {
            case "today":
                $time = [Carbon::now()->subHours(24)->toDateTimeString(), Carbon::now()->toDateTimeString()];
                break;
            case "week":
                $time = [Carbon::now()->subDays(7)->toDateTimeString(), Carbon::now()->toDateTimeString()];
                break;
            case "month":
                $time = [Carbon::now()->subDays(30)->toDateTimeString(), Carbon::now()->toDateTimeString()];
                break;
            case "year":
                $time = [Carbon::now()->subDays(365)->toDateTimeString(), Carbon::now()->toDateTimeString()];
                break;
            default:
                $time = null;
        }

        return $time;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getAnyById($id)
    {
        $post = $this->posts()->whereId($id)->firstOrFail();

        return $post;
    }

    /**
     * @param null  $type
     * @param array $param
     * @return mixed
     */
    public function getTrending($type = null, $param = [])
    {
        isset($param['timeframe']) ? $param['timeframe'] = $param['timeframe'] : $param['timeframe'] = 'month';

        $posts = $this->posts($type, $param)->with('comments', 'votes', 'category', 'user', 'votes.user')
            ->leftJoin('votes', 'posts.id', '=', 'votes.post_id')
            ->leftJoin('comments', 'posts.id', '=', 'comments.post_id')
            ->select('posts.*', DB::raw('count(comments.post_id)*7 + count(votes.post_id)*5 + posts.views as popular'))
            ->groupBy('posts.id')->with('user')->orderBy('popular', 'desc');

        $posts = $posts->paginate(perPage());

        return $posts;
    }

    /**
     * @param null  $type
     * @param array $param
     * @return mixed
     */
    public function getLatest($type = null, $param = [])
    {
        $posts = $this->posts($type, $param)->with('comments', 'votes', 'category', 'user', 'votes.user')->orderBy('approved_at', 'desc')->paginate(perPage());

        return $posts;
    }

    /**
     * @param null  $type
     * @param array $param
     * @return mixed
     */
    public function getFeatured($type = null, $param = [])
    {
        $posts = $this->posts($type, $param)->whereNotNull('featured_at')->with('comments', 'votes', 'category', 'user', 'votes.user')->orderBy('featured_at', 'desc')->paginate(perPage());

        return $posts;
    }

    /**
     * @param null  $type
     * @param array $param
     * @return mixed
     */
    public function getArchive($type = null, $param = [])
    {
        $posts = $this->posts($type, $param)->latest('approved_at')->get()->groupBy(function ($items)
        {
            return $items->approved_at->format('d-M-y');
        })->toArray();

        return $posts;
    }

    /**
     * @param $news
     * @return mixed
     */
    public function incrementViews($news)
    {
        $news->views = $news->views + 1;
        $news->save();

        return $news;
    }

    /**
     * @param       $term
     * @param null  $type
     * @param array $param
     * @return mixed
     */
    public function search($term, $type = null, $param = [])
    {
        $posts = $this->posts($type, $param)->where(function ($query) use ($term)
        {
            $query->orWhere('title', 'LIKE', '%' . $term . '%')
                ->orWhere('summary', 'LIKE', '%' . $term . '%')
                ->orWhere('url', 'LIKE', '%' . $term . '%');
        })->with('comments', 'votes', 'category', 'user', 'votes.user')->orderBy('title', 'asc')->paginate(perPage());

        return $posts;
    }

    /**
     * @param       $type
     * @param array $input
     * @return mixed
     */
    public function create($type, array $input)
    {
        $post = $this->getNew();
        $post->title = $input['title'];
        $post->summary = Purifier::clean($input['summary']);
        $post->user_id = Auth::user()->id;
        $post->category_id = $input['category'];
        $slug = @Str::slug($input['title']);
        if ( ! $slug)
        {
            $slug = Str::random(9);
        }
        $post->approved_at = Carbon::now();
        if ((bool) autoApprove() == false)
        {
            $post->approved_at = null;
        }
        if ($type == 'question')
        {
            $post->type = 'question';
            $post->url = null;
        }
        elseif ($type == 'news')
        {
            $post->type = 'news';
            $post->url = filter_var($input['url'], FILTER_SANITIZE_URL);
        }
        $post->slug = $slug;
        $post->save();

        return $post;
    }

    /**
     * @param $list
     * @param $param
     * @return mixed
     */
    public function getFeedsForUser($list, $param = [])
    {
        return $this->posts(null, $param)->whereIn('user_id', $list)->paginate(perPage());
    }

    //@todo add filter for admin panel

    /**
     * @param Post  $posts
     * @param array $input
     * @return Post
     */
    public function edit(Post $posts, array $input)
    {
        $post = $posts;
        if (isset($input['delete']))
        {
            $post->flags()->delete();
            $post->comments()->delete();
            $post->reply()->delete();
            $post->votes()->delete();
            $post->collectionItems()->delete();
            $post->delete();

            return $posts;
        }
        $slug = @Str::slug($input['title']);
        if ( ! $slug)
        {
            $slug = Str::random(9);
        }
        $post->title = $input['title'];
        $post->slug = $slug;
        $post->summary = Purifier::clean($input['summary']);
        $post->category_id = $input['category'];
        if ($post->type == 'news')
        {
            $post->url = $input['url'];
        }
        if ((bool) autoApprove() == false)
        {
            $post->approved_at = null;
        }
        $post->update();

        return $post;
    }

    /**
     * @param string $type
     * @return mixed
     */
    public function getApprovalRequired($type = 'news')
    {
        $posts = $this->model->whereType($type)->whereNull('approved_at')->with('comments', 'votes', 'category', 'user', 'votes.user')->orderBy('created_at', 'desc')->paginate(perPage());

        return $posts;
    }

    /**
     * Admin helper, gives output irr-respective of post approved or not
     *
     * @param $id
     * @return mixed
     */
    public function getByIdForAdmin($id)
    {
        $posts = $this->model->whereId($id)->first();

        return $posts;
    }

    /**
     * Admin helper, to edit post irr-respective of post approved or not
     *
     * @param Post  $posts
     * @param array $input
     * @return Post
     */
    public function adminEdit(Post $posts, array $input)
    {
        $post = $posts;
        if (isset($input['delete']))
        {
            $post->flags()->delete();
            foreach ($post->comments()->get() as $comment)
            {
                $comment->votes()->delete();
            }
            $post->comments()->delete();
            foreach ($post->reply()->get() as $reply)
            {
                $reply->votes()->delete();
            }
            $post->reply()->delete();
            $post->votes()->delete();
            $post->collectionItems()->delete();
            $post->delete();

            return $posts;
        }
        $slug = @Str::slug($input['title']);
        if ( ! $slug)
        {
            $slug = Str::random(9);
        }
        $post->title = $input['title'];
        $post->slug = $slug;
        $post->summary = Purifier::clean($input['summary']);
        $post->category_id = $input['category'];
        if ($post->type == 'news')
        {
            $post->url = $input['url'];
        }
        if (isset($input['approve']))
        {
            $post->approved_at = Carbon::now();
        }
        if (isset($input['featured']) && $post->featured_at == null)
        {
            $post->featured_at = Carbon::now();
        }
        elseif ( ! isset($input['featured']))
        {
            $post->featured_at = null;
        }
        $post->update();

        return $post;
    }

    /**
     * @param Post $post
     * @return mixed
     */
    public function findNextPost(Post $post)
    {
        $next = $this->posts()->where('approved_at', '>=', $post->approved_at)
            ->whereNotIn('id', [$post->id])
            ->orderBy('approved_at', 'asc')
            ->first(['id', 'slug', 'title']);

        return $next;
    }

    /**
     * @param Post $post
     * @return mixed
     */
    public function findPreviousPost(Post $post)
    {
        $prev = $this->posts()->where('approved_at', '<=', $post->approved_at)
            ->whereNotIn('id', [$post->id])
            ->orderBy('approved_at', 'desc')
            ->first(['id', 'slug', 'title']);

        return $prev;
    }

    /**
     * @param Post $post
     * @return mixed
     */
    public function findRelatedPosts(Post $post)
    {
        $related = $this->posts()
            ->whereNotIn('id', [$post->id])->with('comments', 'votes', 'category', 'user', 'votes.user')
            ->orWhere(function ($query) use ($post)
            {
                $query->where('title', '%' . $post->title . '%')
                    ->whereNotIn('title', [$post->title]);

            })->orderBy(DB::raw('RAND()'))
            ->take(4)->remember(10)->get();

        return $related;
    }
}