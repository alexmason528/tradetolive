<?php
/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
namespace Hackersnews\Repository;

interface CommentsRepositoryInterface {

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param $postId
     * @param $input
     * @return mixed
     */
    public function create($postId, array $input);

    /**
     * @return mixed
     */
    public function delete($id);
}