<?php
/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */

namespace Hackersnews\Validator;

use Collection;

class CollectionValidator extends Validator {

    protected $createRules = [
        'title'   => ['required', 'min:2'],
        'summary' => ['max:200']
    ];

    public function __construct(Collection $model)
    {
        $this->model = $model;
    }
}