<?php
/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */

namespace Hackersnews\Validator;

use Post;

class NewsValidator extends Validator {

    protected $createRules = [
        'title'    => ['required', 'min:3', 'max:180'],
        'url'      => ['required', 'url', 'Unique:posts'],
        'category' => ['required', 'integer']
    ];

    protected $editRules = [
        'title'    => ['required', 'min:3', 'max:180'],
        'url'      => ['required', 'url'],
        'category' => ['required', 'integer']
    ];

    protected $adminEditRules = [
        'title'    => ['required', 'min:3', 'max:180'],
        'url'      => ['required', 'url'],
        'category' => ['required', 'integer']
    ];

    public function __construct(Post $model)
    {
        $this->model = $model;
    }
}