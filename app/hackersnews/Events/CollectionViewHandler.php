<?php

namespace Hackersnews\Events;

use Hackersnews\Repository\CollectionsRepositoryInterface;
use Illuminate\Session\Store;

class CollectionViewHandler {

    /**
     * @param CollectionsRepositoryInterface $collection
     * @param Store                          $session
     */
    public function __construct(CollectionsRepositoryInterface $collection, Store $session)
    {
        $this->session = $session;
        $this->collection = $collection;
    }

    /**
     * @param $collection
     */
    public function handle($collection)
    {
        if ( ! $this->hasViewedTrick($collection))
        {
            $collection = $this->collection->incrementViews($collection);
            $this->storeViewedTrick($collection);
        }
    }

    /**
     * @param $collection
     * @return bool
     */
    protected function hasViewedTrick($collection)
    {
        return array_key_exists($collection->id, $this->getViewedTricks());
    }

    /**
     * Get the users viewed trick from the session.
     *
     * @return array
     */
    protected function getViewedTricks()
    {
        return $this->session->get('viewed_collection', []);
    }

    /**
     * @param $collection
     */
    protected function storeViewedTrick($collection)
    {

        $key = 'viewed_collection.' . $collection->id;

        $this->session->put($key, time());
    }
}