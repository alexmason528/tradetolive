<?php

/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
class Collection extends Eloquent {

    protected $table = 'collections';

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    /**
     * @return mixed
     */
    public function items()
    {
        return $this->hasMany('CollectionItems', 'collection_id');
    }
}