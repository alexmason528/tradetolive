<?php

/**
 * @author Abhimanyu Sharma <abhimanyusharma003@gmail.com>
 */
Class CollectionItems extends Eloquent {

    /**
     * @return mixed
     */
    public function posts()
    {
        return $this->belongsTo('post', 'post_id');
    }

    /**
     * @return mixed
     */
    public function collection()
    {
        return $this->belongsTo('collection', 'collection_id');
    }
}