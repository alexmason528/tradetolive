var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('styles', function() {
    gulp.src('public/new/css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('public/new/css/'))
});

gulp.task('watch', function () {
  gulp.watch('public/new/css/*.scss',['styles']);
  gulp.watch('public/new/css/inc/*.scss',['styles']);
});

//Watch task
gulp.task('default', ['styles', 'watch']);