(function($){
    var body    = $('body'),
        _window = $(window);

    /************************************** //Module name **************************************/
    $(function(){

       $('#commentForm textarea')
       	.on('focus', function(e){       		
       		$('.comment-write').addClass('active');       		
       	})
       	.on('focusout', function(){
       		$('.comment-write').removeClass('active')
       		if($(this).val()) {
       			$(this).css('padding-top', '0');
       		}
       	});

        $('#menuToggle').on('click', function(){            
            $('body').addClass('menu-open').append('<div id="menu-close" class="menu-overlay"></div');
        })

        $('body').on('click', '#menu-close', function(){
            $(this).remove();
            $('body').removeClass('menu-open');
        });

    });

})(jQuery);